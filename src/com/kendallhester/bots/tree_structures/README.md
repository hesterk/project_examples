////////////////////////////////////////////////////

A "TreeStructured Bot" is essentially an automated bot that mimics the design of a Binary Search Tree.

See /personal_api/api/framework/bots/tree/framework/TreeStructure to see the design behind it.

Each of these bots has a root node, though there is a distinction in behavior between nodes that are meant to be branches and nodes that are meant to be leaves. In the end though, the nodes come together to form a standard BST that allows traversal down from the root node. 

Branches (TreeBranch) are guaranteed to contain 'children' nodes, and behave as a direction-ary guide for the tree logic. These classes are guaranteed to have a validation method that will, based on the return value of the method, either 
branch off to child-a (considered a success node) or child-b (considered a failure node). These classes also offer a "pre-validate" method that should be used so as not to clutter the validation method. This pre-validate is useful for when extra steps are needed to be taken before determining if we should step further down the tree. 
In each instance of a TreeBranch, outside any validation or return methods, instances of its children nodes are created.

Leaves (or TreeLeaf) behave as ending nodes, containing no children. These only offer an execute method, which is meant to run whatever execution is needed at that point in time.

In the main class of each individual bot, there is a main method used to create & return a new instance of a root node, called 'getBotSpecificRoot()'.
These are often branches themselves, though they do not have to be.

First a root node is created and then returned. Upon return, the root node will run (usually its validation method), and then (whenever applicable),
branch down to TreeBranch. The traversal from node-to-node continues until we make it all the way down the tree to a leaf node, where an execute method run.
After this is done, the main thread will sleep for a small amount of time (determined in the main class of each project), and then we start back at the root node again for the next traversal.
This continues until the user clicks the stop button, or we reach a situation where the logic has forcefully stopped the bot (usually an .end() or .endSafely() call).

NOTE:
One thing you may see through some of these files is improper conditional use (in some execute methods).
This is due to the design of the game client that this script runs off of. The virtual mouse used in this client (which performs all of the actual game clicks for the script) was designed to be imperfect in order to mimic human behavior.
Although this is nice to have, I tend to see users of these bots wanting to see more efficiency.
To take care of that, I've designed some interactions to have multiple ifs (and sometimes duplicated ifs) to run-through (instead of if/else) in order to allow re-evaluation
of the current character state prior to a full tree traversal.

Note that what you see in the code doesn't always represent true game-state. Sometimes conditions change from true to false in fractions of a millisecond, and sometimes the client returns false positive/negative values.
Because there is a thread-sleep after a full tree traversal, sometimes it is best to have duplicated conditionals to double-check prior to moving on to the next node of a tree.

Design:
(all of this is listed in /personal_api/api/framework/bots)

LoopingBot (client provided) -> APILoopingStructure -> TreeStructure -> Mining (main class of example project)

TreeNode (abstract) -> TreeBranch -> ? (any)

TreeNode (abstract) -> TreeLeaf -> ? (any)
 
 ////////////////////////////////////////////////////