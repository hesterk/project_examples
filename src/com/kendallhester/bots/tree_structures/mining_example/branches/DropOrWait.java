package com.kendallhester.bots.tree_structures.mining_example.branches;

import com.kendallhester.bots.tree_structures.mining_example.leaves.DropLeaf;
import com.kendallhester.bots.tree_structures.mining_example.leaves.WaitLeaf;
import com.kendallhester.bots.tree_structures.mining_example.main.Mining;
import com.kendallhester.personal_api.api.framework.bots.tree.framework.TreeBranch;
import com.kendallhester.personal_api.api.framework.bots.tree.framework.TreeNode;
import com.gameclient.game.api.hybrid.local.hud.interfaces.Inventory;

public class DropOrWait extends TreeBranch {

    private final WaitLeaf waiting = new WaitLeaf();
    private final DropLeaf dropleaf = new DropLeaf();

    @Override
    public TreeNode failureTask() {
        return waiting;
    }

    /*
     * Returns true if the inventory has any useless items that we don't want to keep, or if the inventory is full.
     * We don't want to drop the players other items, so we specify which we are searching for.
     */
    @Override
    public boolean validate() {
        return Inventory.containsAnyOf(this.<Mining>getBotObject().getItems()) || Inventory.isFull();
    }

    @Override
    public TreeNode successTask() {
        return dropleaf;
    }

    private boolean anyRocksAvailable() { return !this.<Mining>getBotObject().getRockQuery().results().isEmpty(); }
}
