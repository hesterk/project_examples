package com.kendallhester.bots.tree_structures.mining_example.branches;

import com.kendallhester.bots.tree_structures.mining_example.leaves.MineLeaf;
import com.kendallhester.bots.tree_structures.mining_example.main.Mining;
import com.kendallhester.personal_api.api.framework.bots.tree.framework.TreeNode;
import com.kendallhester.personal_api.api.framework.bots.tree.framework.TreeBranch;
import com.gameclient.game.api.hybrid.local.hud.interfaces.Inventory;

/**
 * This is the root node of the tree.
 * I create new instances of the child-nodes below that I would want to branch off to (similar to when I created the root
 * in the main class), and then return those instances depending on the result of the validate() call.
 */
public class CheckLocation extends TreeBranch {
    private final DropOrWait droporwait = new DropOrWait();
    private final MineLeaf mineleaf = new MineLeaf();

    @Override
    public TreeNode failureTask() {
        return droporwait;
    }

    /*
     * Determines which task I will go to. the getRockQuery() call that I have beliw is a GameObjects query.
     * This query basically searches all of the loaded GameObjects near the character, and passes in a specific color and action as filters to be put into
     * a stream. Then it returns the results of that stream.
     *
     * I only want to return true if we aren't already in a dropping state, if the inventory isn't full, and if there are rocks available.
     * If all of this is true, we should branch off and make the character mine another ore.
     */
    @Override
    public boolean validate() {
        return !this.<Mining>getBotObject().getRockQuery().results().isEmpty()
                && !this.<Mining>getBotObject().isDropping()
                && !Inventory.isFull();
    }

    /*
     * returns the MineLeaf class instance I created earlier.
     */
    @Override
    public TreeNode successTask() {
        return mineleaf;
    }

}
