package com.kendallhester.bots.tree_structures.mining_example.main;

import com.kendallhester.personal_api.api.framework.bots.universal.ui.APIBotUI;
import com.kendallhester.personal_api.api.framework.utils.formatting.UniversalFormat;

public class MiningController extends APIBotUI {

    private int ores = 0;

    public MiningController(Mining bot) {
        super(bot);
    }

    @Override
    protected void updateBotSpecificValues() {
        ores = this.<Mining>getBotObject().getOres();
    }

    @Override
    protected void updateBotSpecificElements() {
        customTracker1_LL.setText(String.format("%s (%s/hr)",
                UniversalFormat.format(ores),
                UniversalFormat.formatPerHour(online, ores))
        );
    }

    @Override
    protected void botSpecificInitialize() {
        start_BT.setDisable(false);
    }

    @Override
    protected void uiSpecificStart() {
        // none
    }

    @Override
    protected String getResourceStream() {
        return null;
    } // use the default stream

    @Override
    protected void autoFill() { // no auto-filling needed.
        // none
    }
}
