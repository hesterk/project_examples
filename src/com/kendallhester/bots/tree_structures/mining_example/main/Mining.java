package com.kendallhester.bots.tree_structures.mining_example.main;

import com.kendallhester.bots.tree_structures.mining_example.branches.CheckLocation;
import com.kendallhester.personal_api.api.framework.bots.looping.BotType;
import com.kendallhester.personal_api.api.framework.bots.tree.framework.TreeStructure;
import com.kendallhester.personal_api.api.framework.bots.tree.framework.TreeNode;
import com.kendallhester.personal_api.api.framework.utils.formatting.UniversalFormat;
import com.kendallhester.personal_api.api.framework.utils.misc.MathTools;
import com.kendallhester.personal_api.api.game.player.LocalPlayer;
import com.gameclient.game.api.hybrid.local.Skill;
import com.gameclient.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.gameclient.game.api.hybrid.queries.GameObjectQueryBuilder;
import com.gameclient.game.api.hybrid.region.GameObjects;
import com.gameclient.game.api.script.framework.listeners.events.MessageEvent;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.awt.*;
import java.util.Set;


/**
 this is a very simple mining script.
 It will search for and interact with an item called "Rock" to mine iron ores, and then drop them whenever necessary.
 */
public class Mining extends TreeStructure {

    private final static Color iron_ore = new Color(32, 17, 14);
    private final static Color empty_rock = new Color(48, 48, 48);
    private final Set<String> items = Set.of("Iron ore", "Uncut sapphire", "Uncut emerald", "Uncut ruby", "Uncut diamond");
    private int ores = 0;
    private boolean dropping = false;

    /*
     * This is where I create a new tree node to be used as my root.
     */
    @Override
    protected TreeNode getBotSpecificRoot() {
        return new CheckLocation();
    }




    @Override
    public void botSpecificSkillEventHandling(Skill skill, int change, boolean xpEvent) {
        if (skill.equals(Skill.MINING)) ores++;
    }

    /*
     * An onStart() method is defaulted in every class. This is the method that runs before any logic in your loops run.
     * Because I have designed my own structure that is meant to be scalable, there is now a botSpecificStartup() that runs inside of the
     * onStart() method. In here (for this bot, at least), I am setting the range of the sleep timer that happens after the main thread completes one iteration.
     * Although you don't see a loop method in this class, if you go to TreeStructure you will see where to loop is designed.
     */
    @Override
    protected void botSpecificStartup() {
        setLoopDelay(100,200);
    }

    @Override
    protected void botSpecificStop() {
        setStatus(String.format("Total ores mined this session: %s (%s per hour, avg.)",
                UniversalFormat.format(ores),
                UniversalFormat.formatPerHour(getOnlineRuntime(), ores)));
    }

    @Override
    protected void addBooleans() {
        // no need
    }

    @Override
    public BotType getBotType() {
        return BotType.MINING;
    }

    public int getOres() { return ores; }
    public boolean isDropping() { return dropping; }
    public void setDropState(boolean val) { dropping = val; }
    public Set<String> getItems() { return items; }
    public GameObjectQueryBuilder getRockQuery() {
        return GameObjects.newQuery()
                .names("Rocks")
                .filter(o -> LocalPlayer.distanceTo(o) < 3)
                .actions("Mine")
                .colorSubstitutions(empty_rock, iron_ore);
    }

    public Comparator<SpriteItem> getSorter() {
        double chance = MathTools.randDouble(0.00, 0.100);
        return chance > getSortingPair().getRight() ? getOppositeSort() : getSortingPair().getLeft();
    }

    @Override
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        if (botInterfaceProperty == null)
            botInterfaceProperty = new SimpleObjectProperty<>(new MiningController(this));
        return botInterfaceProperty;
    }

    /*
     * This is an eventhandler that is custom to the client, and it is used to track messages received in the character's chatbox.
     */
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        // not needed.
    }
}
