package com.kendallhester.bots.tree_structures.mining_example.leaves;

import com.kendallhester.personal_api.api.framework.utils.misc.Process;
import com.kendallhester.personal_api.api.framework.bots.tree.framework.TreeLeaf;
import com.kendallhester.personal_api.api.game.player.LocalPlayer;
import com.gameclient.game.api.hybrid.entities.GameObject;
import com.gameclient.game.api.hybrid.local.Skill;
import com.gameclient.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.gameclient.game.api.script.Execution;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * This class is the mining interaction leaf. This node interacts with the rock in order to mine the ore.
 */
public class MineLeaf extends TreeLeaf {

    @Override
    public void execute() {
        if (getBot().getSkillListener().getCurrentLevelOf(Skill.MINING) >= 82) {
            getBot().endSafely();
            return;
        }

        GameObject rock = Objects.requireNonNull(getMiningBot()).getRockQuery().results().nearest();
        if (rock != null && rock.isValid()) {
            if (Process.delayUntil(
                    () -> rock.interact("Mine"),
                    LocalPlayer::isMoving,
                    "Mining rock(s).",
                    2000,
                    3600
            ))
                Execution.delayWhile(() -> LocalPlayer.isMoving() && rock.isValid(), 7000);
        } else { //checking if the rock is valid
            setStatus("The queried rock isn't valid. Can't mine it, so moving on.");
        }

        if (getBot().getRuntimeAs(TimeUnit.MINUTES) > 240 || !Interfaces.newQuery().texts("has been updated").visible().results().isEmpty()) {
            setStatus("Hit our runtime limit. Logging off.");
            getBot().endSafely();
        }
    }
}
