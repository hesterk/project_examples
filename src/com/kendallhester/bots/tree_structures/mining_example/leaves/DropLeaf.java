package com.kendallhester.bots.tree_structures.mining_example.leaves;

import com.kendallhester.personal_api.api.framework.utils.misc.Process;
import com.kendallhester.bots.tree_structures.mining_example.main.Mining;
import com.kendallhester.personal_api.api.framework.bots.tree.framework.TreeLeaf;
import com.kendallhester.personal_api.api.game.interfaces.OSInventory;
import com.kendallhester.personal_api.api.game.player.Actions;
import com.gameclient.game.api.hybrid.local.hud.interfaces.SpriteItem;

import java.util.Comparator;
import java.util.Set;

public class DropLeaf extends TreeLeaf {

    @Override
    public Mining getBot() { return getBotObject(); }


    @Override
    public void execute() {
        /*
         * A SpriteItem is any kind of item that can be interacted with that is not attached to the ground (or an NPC).
         * So this item can be in the character's bank, inventory, interface, etc.
         * If a SpriteItem is on the ground (i.e. not owned by any other interface or NPC), it is called a GroundItem.
         * Below, I am dropping all SpriteItems in my inventory that match any of the names listed above.
         */

        getBot().setDropState(true);
        setStatus("Dropping all ores.");
        Comparator<SpriteItem> sorter = getBot().getSorter();
        if (Process.delayUntil(
                () -> Actions.dropAll(true, sorter, getBot().getItems()),
                () -> !OSInventory.hasAnyOf(getBot().getItems()),
                2000
        ))
            getBot().setDropState(false);

    }
}
