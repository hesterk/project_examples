package com.kendallhester.bots.tree_structures.mining_example.leaves;

import com.kendallhester.bots.tree_structures.mining_example.main.Mining;
import com.kendallhester.personal_api.api.framework.bots.tree.framework.TreeLeaf;
import com.kendallhester.personal_api.api.framework.utils.misc.MathTools;
import com.kendallhester.personal_api.api.game.player.LocalPlayer;
import com.gameclient.game.api.hybrid.entities.GameObject;
import com.gameclient.game.api.hybrid.local.Camera;
import com.gameclient.game.api.hybrid.local.Skill;
import com.gameclient.game.api.hybrid.local.hud.interfaces.Inventory;
import com.gameclient.game.api.hybrid.region.GameObjects;
import com.gameclient.game.api.script.Execution;

/**
 * This node is an example of an "unnecessary class". It isn't needed for functionality, but it
 * creates a delay (and does random interactions, like turning) while the character waits for a new rock to load in.
 * This makes the script seem more human-like.
 */
public class WaitLeaf extends TreeLeaf {

    @Override
    public Mining getBot() { return getBotObject(); }

    public void execute() {
        if (getBot().checkTurn()) {
            if (MathTools.randInt(0, 13) > 8) {
                setStatus("Turning camera to specific rock while we wait.");
                GameObject rocks = GameObjects
                        .newQuery()
                        .names("Rocks")
                        .on(LocalPlayer.getLocation().derive(-1, 0))
                        .results().first();
                if (rocks != null) {
                    if (!rocks.isVisible()) Camera.concurrentlyTurnTo(rocks);
                }
            }
        }
        int xp = getBot().getSkillListener().getExperienceGainedFor(Skill.MINING);
        Execution.delayUntil(
                () ->
                        !LocalPlayer.isMoving()
                        && (Inventory.contains("Iron ore") || xp != getBot().getSkillListener().getExperienceGainedFor(Skill.MINING)),
                3000);
    }

}
