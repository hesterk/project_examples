package com.kendallhester.personal_api.api.framework.utils.formatting;
import java.util.Arrays;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

    private static final Set<String> IGNORED_WORDS = Set.of("a", "an", "the", "of", "on", "to", "for", "by", "at");
    private static final String EMPTY_STRING = "";

    /**
     * Validation
     */
    public static boolean exists(String s) {
        return !(isNullOrEmpty(s) || isBlank(s));
    }
    public static boolean isNullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }
    public static boolean isBlank(String s) {
        if (s == null) return false; // we want strictly blank strings
        return s.equals(EMPTY_STRING) || s.trim().isEmpty();
    }
    public static String getEmptyString() { return EMPTY_STRING; }

    public static boolean equals(String a, String b) { // could prob use Objects.equals()
        if (a == null && b == null)
            return true;
        else if (a == null || b == null)
            return false;
        else return a.equals(b);
    }

    public static boolean equalsIgnoreCase(String a, String b) {
        if (a == null && b == null) return true;
        else if (a == null || b == null) return false;
        else return a.equalsIgnoreCase(b);
    }

    public static boolean equalsAny(String match, String... list) {
        return equalsAny(true, match, list);
    }

    public static boolean equalsAny(boolean matchCase, String match, String... list) {
        if (list.length > 0) {
            if (match != null) {
                return matchCase ? Arrays.stream(list).anyMatch(i -> equals(match, i))
                        : Arrays.stream(list).anyMatch(i -> equalsIgnoreCase(match, i));
            }
        }
        return false;
    }

    public static String getFirstMatch(Pattern p, String s) {
        Matcher m = p.matcher(s);
        if (m.find()) return m.group();
        return null;
    }

    /*
     * Operations
     */
    public static int parseInt(String val) {
        int value = -1;
        val = removeNonAlphaNumeric(val);
        if (exists(val) && val.length() < 10) {
            try {
                value = Integer.parseInt(val);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    public static String getFirstWord(String s) {
        int index = s.indexOf(' ');
        return index > -1 ? s.substring(0, index).trim() : s;
    }

    public static String capitalize(String s) { // full word
        if (exists(s)) {
            return s.toUpperCase();
        }
        return s;
    }

    public static String decapitalize(String s) { // full word
        if (exists(s)) {
            return s.toLowerCase();
        }
        return s;
    }

    public static String formatCapitalize(String s) { // first letter only
        if (exists(s)) {
            return s.substring(0, 1).toUpperCase() + s.substring(1);
        }
        return s;
    }

    public static String formatDecapitalize(String s) { // first letter only
        if (exists(s)) {
            return s.substring(0, 1).toLowerCase() + s.substring(1);
        }
        return s;
    }

    public static String toTitleCase(String s) { // first letter every word
        if (exists(s)) {
            String si = s.trim();
            String[] arr = si.split(" ");
            StringBuilder sb = new StringBuilder();

            for (String value : arr) {
                sb.append(Character.toUpperCase(value.charAt(0)))
                        .append(value.substring(1)).append(" ");
            }
            return sb.toString().trim();
        }
        return s;
    }

    public static String toFormattedTitleCase(String s) { // ignore common middle words that should be lowercase
        if (exists(s)) {
            String si = s.trim();
            String[] arr = si.split(" ");
            StringBuilder sb = new StringBuilder();

            for (String value : arr) {
                if (!IGNORED_WORDS.contains(value) || Arrays.asList(arr).indexOf(value) == 0) {
                    sb.append(Character.toUpperCase(value.charAt(0)))
                            .append(value.substring(1)).append(" ");
                }
                else sb.append(decapitalize(value)).append(" ");
            }
            return sb.toString().trim();
        }
        return s;
    }

    public static String removeFirstAndLastChar(String s) {
        if (exists(s) && s.length() > 2) {
            return s.substring(1, s.length() - 1);
        }
        return s;
    }

    /**
     * Removes all non-numeric characters from a string, ignoring periods and hyphens
     * @param s - string that needs to be filtered
     * @return string 's' with only numeric and special characters
     */
    public static String removeNonNumericSpecial(String s) {
        if (exists(s)) {
            return s.replaceAll("[^\\d.-]", "");
        }
        return s;
    }

    public static String removeNonAlphaNumeric(String s) {
        if (exists(s)) {
            return s.replaceAll("[^a-zA-Z0-9-]", "");
        }
        return s;
    }

    public static Pattern toPattern(String s) {
        String s1 = s.replace("(", "\\(");
        s1 = s1.replace(")", "\\)");
        String s2 = "^(?i)" + s1 + "$";
        return Pattern.compile(s2);
    }

    public static String getPrettyPrintedArray(String[] s) {
        return s != null ? removeFirstAndLastChar(Arrays.toString(s)) : "";
    }
}
