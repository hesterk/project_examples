package com.kendallhester.personal_api.api.framework.utils.formatting;

import com.kendallhester.personal_api.api.framework.utils.misc.MathTools;
import com.gameclient.game.api.hybrid.util.calculations.CommonMath;

import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


//System update in: MM:SS
public class UniversalFormat {

    private final static String timePattern = "^(\\d{2}:)?(\\d{2}:)?\\d{2}:\\d{2}$";

    public static String getTimePatternString() { return timePattern; }
    public static Pattern getTimePattern() { return Pattern.compile(timePattern); }

    public static long getSecondsAsMillisecs(long seconds) {
        return 1000 * seconds;
    }
    public static long getMinutesAsMillisecs(long minutes) {
        return getSecondsAsMillisecs(minutes * 60);
    }

    /**
     * Reminder: Anything over 596 should not be used in ExecutionDelay
     * @param hours - hours time variable
     * @return - Converts time represented in hours into milliseconds
     */
    public static long getHoursAsMillisecs(long hours) { return getMinutesAsMillisecs(hours * 60); }
    private static long getDaysAsMillisecs(long days) { return getHoursAsMillisecs(days * 24);}
    /**
     * Anything over 596 hours can't be used in ExecutionDelay
     * @param hrs - hours part of time value
     * @param mins - minutes part of time value
     * @param secs - seconds part of time value
     * @return
     */
    public static long getTimeAsMillisecs(long hrs, long mins, long secs) {return getHoursAsMillisecs(hrs) + getMinutesAsMillisecs(mins) + getSecondsAsMillisecs(secs);}

    /**
     * Parses a time string and converts it to long representing that time in milliseconds
     * @param time - time string accepted in format dd:HH:mm:ss
     * @return long value
     */
    public static long parseTime(String time) {
        String[] split = time.split(":");
        if (split.length < 2) return -1;

        int dayIndex = -1, hrIndex = -1, minIndex = -1, secIndex = -1;

        if (split.length == 4) { dayIndex = 0; hrIndex = 1; minIndex = 2; secIndex =3; }
        else if (split.length == 3) { hrIndex = 0; minIndex = 1; secIndex = 2; }
        else if (split.length == 2) { minIndex = 0; secIndex = 1; }

        int days = dayIndex != -1 ? Integer.parseInt(split[dayIndex]) : 0;
        int hours = hrIndex != -1 ? Integer.parseInt(split[hrIndex]) : 0;
        int minutes = Integer.parseInt(split[minIndex]);
        int seconds = Integer.parseInt(split[secIndex]);
        return getDaysAsMillisecs(days) + getHoursAsMillisecs(hours) + getMinutesAsMillisecs(minutes) + getSecondsAsMillisecs(seconds);
    }

    public static String convertToTimeString(int time) {
        String days = "", hours, minutes, seconds;

        int val = time;
        if (val > getDaysAsMillisecs(1)) {
            days = "01";
            val = val % (int) getDaysAsMillisecs(1);
        }
        val = val / (int) getHoursAsMillisecs(1);
        hours = val < 10 ? String.format("0%d", val) : Integer.toString(val);

        val = time % (int) getHoursAsMillisecs(1);
        val = val / (int) getMinutesAsMillisecs(1);
        minutes = val < 10 ? String.format("0%d", val) : Integer.toString(val);

        val = time % (int) getHoursAsMillisecs(1);
        val = val % (int) getMinutesAsMillisecs(1);
        val = val / (int) getSecondsAsMillisecs(1);
        seconds = val < 10 ? String.format("0%d", val) : Integer.toString(val);

        return StringUtil.exists(days) ? String.format("%s:%s:%s:%s", days, hours, minutes, seconds)
                : String.format("%s:%s:%s", hours, minutes, seconds);
    }

    public static String smartFormat(Number i, int threshold) {
        if (i.longValue() > Math.pow(10, threshold)) return format(i, 2);
        return format(i);
    }
    public static String format(Number i) {
        return NumberFormat.getInstance().format(i);
    }
    public static String format(Number i, int precision) { return formatShorthand(i, precision); }
    private static String formatShorthand(Number num, int precision) {
        if (precision < 0) return UniversalFormat.format(num); // this should never happen.
        double val = num.doubleValue();
        if (Math.abs(num.longValue()) >= 1000) {
            int length = String.valueOf(num).length();
            int power = (int) Math.floor((double) length / 3) * 3;
            char n = '\u0000';
            switch (power) {
                case 15: n = 'Q'; break;
                case 12: n = 'T'; break;
                case 9: n = 'B'; break;
                case 6: n = 'M'; break;
                case 3: default: n = 'K'; break;
            }
            int divisor = (int) Math.pow(10, power);
            if (Math.abs(num.longValue()) >= divisor) {
                val = num.doubleValue() / divisor;
                val = MathTools.round(val, precision);
                return String.format("%s%c", UniversalFormat.format(val), n);
            }
        }
        return UniversalFormat.format(MathTools.round(val, precision));
    }

    public static String formatAsGp(Number i) { return formatAsGp(i, 1);}
    public static String formatAsGp(Number num, int precision) {
        return formatShorthand(num, precision) + " gp";
    }

    public static String smartFormatPerHour(long time, Number val, int threshold) {
        int perRateValue = (int) CommonMath.rate(TimeUnit.HOURS, time, val.longValue());
        return Math.abs(perRateValue) >= Math.pow(10, threshold) ? format(perRateValue, 2) : format(perRateValue);
    }
    public static String formatPerHour(long time, double val) { return formatPerRate(TimeUnit.HOURS, time, val);}
    public static String formatPerHour(long time, double val, int precision) { return formatPerRate(TimeUnit.HOURS, time, val, precision); }
    public static String formatPerRate(TimeUnit rate, long time, double val) { return formatPerRate(rate, time, val, false); }
    public static String formatPerRate(TimeUnit rate, long time, double val, boolean shortHand) {
        int perRateValue = (int) CommonMath.rate(rate, time, val);
        return shortHand ? formatShorthand(perRateValue, 1) : format(perRateValue);
    }
    public static String formatPerRate(TimeUnit rate, long time, double val, int precision) {
        int perRateValue = (int) CommonMath.rate(rate, time, val);
        return formatShorthand(perRateValue, precision);
    }
    public static String formatAsGpPerHour(long time, double gp, int precision) {
        return String.format("%s gp", formatPerHour(time, gp, precision));
    }

    public static String getTimeTil(Number rate, Number remaining) {
        return convertToTimeString((int)(3600000 * (remaining.doubleValue() / rate.doubleValue())));
    }
    public static int getTimeTilAsValue(Number rate, Number remaining) {
        return (int)(3600000 * (remaining.doubleValue() /rate.doubleValue()));
    }
}
