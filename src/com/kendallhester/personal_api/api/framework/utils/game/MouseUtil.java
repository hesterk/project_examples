package com.kendallhester.personal_api.api.framework.utils.game;

import com.kendallhester.personal_api.api.framework.utils.misc.MathTools;
import com.kendallhester.personal_api.api.framework.utils.misc.Sorting;
import com.kendallhester.personal_api.api.game.player.LocalPlayer;
import com.gameclient.game.api.hybrid.Environment;
import com.gameclient.game.api.hybrid.entities.Entity;
import com.gameclient.game.api.hybrid.entities.details.Interactable;
import com.gameclient.game.api.hybrid.entities.details.Locatable;
import com.gameclient.game.api.hybrid.input.Keyboard;
import com.gameclient.game.api.hybrid.input.Mouse;
import com.gameclient.game.api.hybrid.local.Camera;
import com.gameclient.game.api.hybrid.local.Screen;
import com.gameclient.game.api.hybrid.local.hud.*;
import com.gameclient.game.api.hybrid.local.hud.interfaces.Bank;
import com.gameclient.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.gameclient.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.gameclient.game.api.hybrid.location.Area;
import com.gameclient.game.api.hybrid.location.Coordinate;
import com.gameclient.game.api.hybrid.queries.results.SpriteItemQueryResults;
import com.gameclient.game.api.hybrid.util.calculations.Random;
import com.gameclient.game.api.osrs.local.hud.interfaces.ControlPanelTab;
import com.gameclient.game.api.osrs.local.hud.interfaces.OptionsTab;
import com.gameclient.game.api.script.Execution;

import java.util.List;
import java.util.Objects;

import static java.awt.event.KeyEvent.VK_SHIFT;

public class MouseUtil {

    public static boolean forceClick(InterfaceComponent i) {
        return i != null && i.isValid() && i.isVisible() && click(i);
    }

    // TODO: Interact && Click fixes
    public static boolean click(Interactable i) {
        if (i != null) {
            if (Mouse.move(i)) {
                Execution.delay(0, 55, 25);
                if (Mouse.click(Mouse.Button.LEFT)) {
                    return MouseUtil.wasClickSuccessful(i);
                }
            }
            return false;
        }
        return true;
    }

    public static boolean click(SpriteItemQueryResults items, boolean shift) {
        if (Bank.isOpen()) {
            Bank.close();
            return false;
        }

        if (!Bank.isOpen() && !ControlPanelTab.INVENTORY.open()) {
            return false;
        }

        if (shift) {
            if (!OptionsTab.AllSettings.SHIFT_DROPPING.isEnabled() && !OptionsTab.AllSettings.SHIFT_DROPPING.toggle()) {
                Environment.getLogger().debug("Failed to enable shift clicking for click()");
                // maybe return interact method instead of just outright failing.
                return false;
            }

            if (items.isEmpty() && Keyboard.isPressed(VK_SHIFT)) {
//                Environment.getLogger().warn("Releasing shift.");
                Keyboard.releaseKey(VK_SHIFT);
                Execution.delay(0, 80, 35);
                return true;
            }

            if (!Keyboard.isPressed(VK_SHIFT)) {
                Keyboard.pressKey(VK_SHIFT);
                Execution.delay(0, 80, 35);
            }
        }

        if (!shift && Keyboard.isPressed(VK_SHIFT)) {
            Keyboard.releaseKey(VK_SHIFT);
        }

        List<SpriteItem> results = items.sort(Sorting.getInvSort(MathTools.randInt(1, 4))).asList();
        if (!results.isEmpty()) {
            for (SpriteItem i : results) {
                try {
                    click(i);
                } catch (StackOverflowError e) {
                    Environment.getLogger().warn("SOFE error");
                }
            }
            return true;
        }
        return false;
    }

    public static boolean wasClickSuccessful(Interactable i) {
        if (i != null) {
            Mouse.CrosshairState x = Mouse.getOverlayCrosshairColorFor(i);
            try {
                return Mouse.CrosshairState.VARIES.equals(x) || Objects.equals(Mouse.getCrosshairState(), x);
            } catch (IndexOutOfBoundsException e) {
                Environment.getLogger().warn("IOOB for click verification.");
                return false;
            }
        }
        return false;
    }

    public static boolean mouseWheelTurnTo(Locatable i){
        if ( i != null ) {
            Coordinate target = i.getPosition();
            Coordinate pC = LocalPlayer.getLocation();

            if (target != null) {
                int Dx = target.getX() - pC.getX();
                int Dy = target.getY() - pC.getY();
                int Yaw = Camera.getYaw();

                int Beta = (int) (Math.atan2(-Dx, Dy) * 180 / Math.PI); //atan2 is in radians so 180/PI wil transform it to degrees, like the game.
                if (Beta < 0) Beta = 360 + Beta;

                int deltaYaw = Beta - Yaw;

                if (deltaYaw > 180) {
                    deltaYaw = deltaYaw - 360;
                } else if (deltaYaw < -180) {
                    deltaYaw = deltaYaw + 360;
                }

                int deltaMouseMoveX = (int) (-deltaYaw * 2.5);

                Area hoverArea = new Area.Circular(LocalPlayer.getLocation(), 3);
                hoverArea.getRandomCoordinate().hover();

                if (Mouse.isPressed(Mouse.Button.WHEEL) || Mouse.press(Mouse.Button.WHEEL)) {
                    if (Mouse.move(new InteractablePoint((int) (Mouse.getPosition().getX() + deltaMouseMoveX), (int) (Mouse.getPosition().getY() + MathTools.randInt(-10, 10)))))
                        return Mouse.release(Mouse.Button.WHEEL) && target.isVisible();
                }
            }
        }
        return false;
    }

    private static boolean isMouseOnScreen() {
        InteractableRectangle bounds = Screen.getBounds();
        if (bounds != null)
            return bounds.contains(Mouse.getPosition());
        return true;
    }

    public static boolean mouseOffScreen() {
        if (isMouseOnScreen())
            return Mouse.move(new InteractablePoint(getOffScreenX(), getOffScreenY()));
        return true;
    }

    private static int getOffScreenX() {
        if (Random.nextBoolean()) {
            InteractableRectangle bounds = Screen.getBounds();
            if (bounds != null) {
                final int width = bounds.width;
                return width + MathTools.randInt(20, 50);
            }
        }
        return -MathTools.randInt(20, 50);
    }

    private static int getOffScreenY() {
        if (Random.nextBoolean()) {
            InteractableRectangle bounds = Screen.getBounds();
            if (bounds != null) {
                final int height = bounds.height;
                return height + MathTools.randInt(20, 50);
            }
        }
        return -MathTools.randInt(20, 50);
    }
}
