package com.kendallhester.personal_api.api.framework.utils.game;

import com.kendallhester.personal_api.api.game.player.LocalPlayer;
import com.gameclient.game.api.hybrid.RuneScape;
import com.gameclient.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.gameclient.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.gameclient.game.api.hybrid.queries.results.InterfaceComponentQueryResults;
import com.gameclient.game.api.osrs.local.hud.interfaces.ControlPanelTab;
import com.gameclient.game.api.script.Execution;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.RejectedExecutionException;

public class Game {

    private static final InterfaceComponent allTab = Interfaces.getAt(162,4);
    private static final InterfaceComponent gameTab = Interfaces.getAt(162,8);
    private static final InterfaceComponent publicTab = Interfaces.getAt(162,13);
    private static final InterfaceComponent friendsTab = Interfaces.getAt(162,18);
    private static final InterfaceComponent clanTab = Interfaces.getAt(162,23);
    private static final InterfaceComponent tradeTab = Interfaces.getAt(162,28);
    private static final InterfaceComponent reportButton = Interfaces.getAt(162,33);

    private static final List<InterfaceComponent> chatTabs = List.of(allTab, gameTab, publicTab, friendsTab, clanTab, tradeTab, reportButton);

    public static boolean isLoggedIn(boolean hybrid) {
        boolean status = false;
        if (hybrid) {
            status = RuneScape.isLoggedIn();
        }
        return status || isLoggedIn();
    }

    public static boolean isLoggedIn() {
        try {
            return chatTabsHaveLoaded()
                    || !getChatGameComponents().isEmpty()
                    || getLobbyPlayButton() != null;
        } catch (RejectedExecutionException e) {
            return false; // by default, no.
        }
    }

    public static boolean isLoggedOut() { // using for breaks and such
        return !isLoggedIn();
    }

    private static boolean chatTabsHaveLoaded() {
        return chatTabs.stream().filter(Objects::nonNull).anyMatch(i -> i.isValid() && i.isVisible());
    }

    private static InterfaceComponentQueryResults getChatGameComponents() {
        return Interfaces.newQuery().containers(162)
                .types(InterfaceComponent.Type.CONTAINER)
                .actions("Switch tab").filter(i -> i.getActions().size() > 2)
                .visible().results();
    }

    private static InterfaceComponent getLobbyPlayButton() {
        return Interfaces.newQuery().containers(378)
                .types(InterfaceComponent.Type.CONTAINER)
                .actions("Play").visible().results().first();
    }

    public static boolean logout() {
        if (!ControlPanelTab.LOGOUT.isOpen() && ControlPanelTab.LOGOUT.isOpenable()) ControlPanelTab.LOGOUT.open();
        if ((RuneScape.isLoggedIn() || isLoggedIn()) && LocalPlayer.hasFreeClick() && RuneScape.logout())
            Execution.delayUntil(() -> !isLoggedIn() || Game.isLoggedOut(), 2000);

        return RuneScape.isLoggedIn() && isLoggedOut();
    }
}
