package com.kendallhester.personal_api.api.framework.utils.game.operations.base;


import com.gameclient.game.api.hybrid.Environment;

import java.util.Map;
import java.util.Optional;

public abstract class OperationSelectorModel {
    private Operation opTarget;

    public abstract Map<String, Operation> getOperationMap();

    private static final Operation DEFAULT_OP = Operation.newBuilder()
            .addSubOp(() -> Environment.getLogger().info("Invalid activity selected"))
            .build();

    void setOperationByName(String opName) {
        Environment.getLogger().info("Switching to operation " + opName);
        Environment.getLogger().info("Switching to activity " + opName);
        this.opTarget = getOperationMap().getOrDefault(opName, DEFAULT_OP);
        Environment.getLogger().info(opTarget);
    }

    String[] getActivityKeys() {
        return getOperationMap().keySet().stream().sorted().toArray(String[]::new);
    }

    Optional<Operation> getOperation() {
        return Optional.ofNullable(opTarget);
    }
}
