package com.kendallhester.personal_api.api.framework.utils.game;

import com.kendallhester.personal_api.api.framework.utils.misc.MathTools;
import com.gameclient.game.api.hybrid.entities.LocatableEntity;
import com.gameclient.game.api.hybrid.entities.details.Locatable;
import com.gameclient.game.api.hybrid.local.Camera;
import com.gameclient.game.api.hybrid.location.Coordinate;

import static com.kendallhester.personal_api.api.framework.utils.game.MouseUtil.mouseWheelTurnTo;

public class OSCamera {

    public static boolean turnCamera() {
        return Camera.turnTo(MathTools.randInt(1,359), MathTools.randDouble(0, 0.66));
    }
}
