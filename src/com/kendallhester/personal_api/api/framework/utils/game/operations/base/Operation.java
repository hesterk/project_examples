package com.kendallhester.personal_api.api.framework.utils.game.operations.base;

import com.kendallhester.personal_api.api.framework.bots.looping.APILoopingStructure;
import com.kendallhester.personal_api.api.framework.utils.game.Game;
import com.kendallhester.personal_api.api.game.player.LocalPlayer;
import com.gameclient.game.api.hybrid.Environment;
import com.gameclient.game.api.hybrid.location.Area;
import com.gameclient.game.api.hybrid.location.Coordinate;
import com.gameclient.game.api.hybrid.util.StopWatch;
import com.gameclient.game.api.script.Execution;

import java.time.Duration;
import java.util.*;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Operation implements Runnable {
    
    private final List<BooleanSupplier> preConditions;
    private final List<Runnable> activities;
    private final StopWatch stopWatch;
    private int iteration;
    private boolean printName;
    private Optional<String> name;
    private Optional<Operation> parent;
    private long pauseMillis;
    private final Optional<OperationConfigModel> configModel;

    // private because you should only create activities through Operation.newBuilder()...build().
    private Operation(List<BooleanSupplier> preConditions,
                     List<Runnable> activities,
                     Optional<String> name,
                     long pauseMillis,
                     Optional<OperationConfigModel> configModel) {
        // Pass down context information to any child activities
        activities = activities.stream()
                .map(runnable -> {
                    if (!Operation.class.isInstance(runnable)) {
                        return runnable;
                    }

                    Operation op = Operation.class.cast(runnable);
                    op.setParent(this);
                    return op;
                })
                .collect(Collectors.toList());

        this.preConditions = preConditions;
        this.activities = activities;
        this.stopWatch = new StopWatch(); stopWatch.start();
        this.iteration = 1;
        this.name = name;
        this.parent = Optional.empty();
        this.pauseMillis = pauseMillis;
        this.configModel = configModel;
    }

    private Operation setParent(Operation parent) {
        this.parent = Optional.of(parent);
        return this;
    }

    /**
     * Finds the nearest parent that has a config model, returning it.
     */
    public Optional<OperationConfigModel> getConfigModel() {
        if (configModel.isPresent()) return configModel;
        return parent.flatMap(Operation::getConfigModel);
    }

    /**
     * Returns the full name of this activity, which is the name of the current activity along with all parent activity
     * names, separated by a space.
     */
    private Optional<String> getFullName() {
        return this.name.map(name -> this.parent.flatMap(Operation::getFullName).map(s -> s + " : ").orElse("") + name);
    }

    private boolean shouldPrintName() {
        return this.printName;
    }

    /**
     * Creates an activity that just performs a runnable
     */
    public static Operation of(Runnable runnable) {
        return Operation.newBuilder().addSubOp(runnable).build();
    }

    /**
     * Creates an activity with default values set. Ideally, activities should be immutable and this should create
     * an Operation.Builder. Laziness prevents me from doing so.
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Performs the activity.  By making an Activity a runnable, plain lambdas may be passed in as subactivities.
     */
    @Override
    public void run() {
        stopWatch.reset(); // Reset so duration is just for a current run.
        iteration = 1;

        while (preConditions.stream().allMatch(BooleanSupplier::getAsBoolean)) {
            if (getFullName().isPresent()) {
                String name = getFullName().get();
                if (iteration == 1) Environment.getLogger().info("Current Activity : " + name);
                if (shouldPrintName()) ((APILoopingStructure) Objects.requireNonNull(Environment.getBot())).setStatus(name);
            }

            for (int i = 0; i < activities.size(); ++i) {
                Runnable runnable = activities.get(i);
                runnable.run();

                if (i == activities.size() - 1) continue;
                if (Operation.class.isInstance(runnable) && Operation.class.cast(runnable).iteration <= 1) continue;
                Execution.delay(pauseMillis, 2 * pauseMillis + 1);
            }

            ++iteration;
        }
    }

    @Override
    public String toString() {
        return getFullName().orElse("Unnamed activity");
    }

    /**
     * Performs a given activity after the current activity.
     */
    public Operation andThen(Operation other) {
        return Operation.newBuilder()
                .addSubOp(this)
                .addSubOp(other)
                .onlyOnce()
                .withoutPausingBetweenActivities()
                .build();
    }

    /**
     * Performs a given activity before the current activity.
     */
    public Operation addPreOp(Operation other) {
        return other.andThen(this);
    }

    /**
     * Builder for building immutable Activity objects.
     */
    public static class Builder {
        private final List<BooleanSupplier> preConditions;
        private final List<Runnable> activities;
        private boolean printName;
        private int maxIterations;
        private Duration maxDuration;
        private Optional<String> name;
        private Optional<OperationConfigModel> configModel;
        private long pauseMillis; // Pause between activities will be [pauseMillis, 2 * pauseMillis]

        Builder() {
            this.preConditions = new ArrayList<>();
            this.activities = new ArrayList<>();
            this.maxIterations = 1;
            this.printName = false;
            this.maxDuration = Duration.ofHours(6);
            this.name = Optional.empty();
            this.configModel = Optional.empty();
            this.pauseMillis = 333;
        }

        /**
         * Builds the completed, immutable Activiity object.
         */
        public Operation build() {
            Operation op = new Operation(preConditions, activities, name, pauseMillis, configModel);
            op.printName = this.printName;
            op.preConditions.add(() -> op.stopWatch.getRuntime() < maxDuration.toMillis());
            op.preConditions.add(() -> op.iteration <= maxIterations);
            op.preConditions.add(() -> !Objects.requireNonNull(Environment.getBot()).isPaused());
            op.preConditions.add(() -> !Objects.requireNonNull(Environment.getBot()).isStopped());
            op.preConditions.add(() -> Game.isLoggedIn(true));

            return op;
        }

        /**
         * Adds an individual task to this activity, which will be performed in order based on the order tasks are
         * added by this method.
         */
        public Builder addSubOp(Runnable activity) {
            activities.add(activity);
            return this;
        }

        /**
         * Adds a subactivity, taking in a keval set of params set in the GUI.
         */
        public Builder addSubOp(Consumer<Map<String, String>> function) {
            activities.add(() -> {
                Map<String, String> map = configModel.map(model -> model.keyValStore).orElse(new HashMap<>());
                function.accept(map);
            });
            return this;
        }

        /**
         * A config model specifies how the UI will appear to a user under their selected activity. This only makes
         * sense to set on a top level activity, as that is what determines the GUI options.
         */
        public Builder withConfigModel(OperationConfigModel configModel) {
            this.configModel = Optional.of(configModel);
            return this;
        }

        /**
         * Adds a name to this activity, just used for logging and optional to include.
         */
        public Builder withName(String name) {
            this.name = Optional.of(name);
            return this;
        }

        public Builder withPauseOf(Duration duration) {
            this.pauseMillis = duration.toMillis();
            return this;
        }

        public Builder shouldPrintName(Boolean state) {
            this.printName = state;
            return this;
        }

        /**
         * Removes the ~1 tick pause between sub-activities. This should only be used in cases where speed is of the
         * top importance (think prayer switches, repeated clicks on a single object, etc.) where it could be assumed
         * a player could theoretically perform the actions without pauses. As a rule of thumb, if its a common thing
         * people use hotkeys for, its probably good to use this method for it.
         */
        public Builder withoutPausingBetweenActivities() {
            return withPauseOf(Duration.ofMillis(0));
        }

        /**
         * Actions will continue to occur some prereq added by this method is false.
         */
        public Builder addPreReq(BooleanSupplier condition) {
            preConditions.add(condition);
            return this;
        }

        /**
         * Adds a prereq, taking in config information a user specified in the GUI.
         */
        public Builder addPreReq(Function<Map<String, String>, Boolean> function) {
            preConditions.add(() -> {
                Map<String, String> map = configModel.map(model -> model.keyValStore).orElse(new HashMap<>());
                return function.apply(map);
            });
            return this;
        }

        /**
         * Sets the maximum amount of time an Activity will run for. This check is performed before each action iteration,
         * so actions with long iteration times may go well beyond the given duration.
         * <p>
         * By default, activities run until a precondition fails.
         */
        public Builder maximumDuration(Duration duration) {
            maxDuration = duration;
            return this;
        }

        /**
         * Sets the maximum number of iterations an Activity will run for.
         * <p>
         * By default, activities run until a precondition fails.
         */
        public Builder maximumTimes(int count) {
            maxIterations = count;
            return this;
        }

        /**
         * Adds an activity to pause for ~600ms.
         */
        public Builder tick() {
            return addSubOp(() -> Execution.delay(572, 614));
        }

        public Builder onlyIfAtPosition(Coordinate pos) {
            return addPreReq(() -> LocalPlayer.isAt(pos));
        }

        public Builder onlyIfInArea(Area area) {
            return addPreReq(() -> LocalPlayer.isContainedIn(area));
        }

        public Builder completeAnimation() {
            sleepUntil(LocalPlayer::isMoving);
            return sleepUntil(LocalPlayer::isMoving);
        }

        public Builder thenSleepWhile(BooleanSupplier condition) {
            return thenSleepWhile(condition, Duration.ofMinutes(2));
        }

        public Builder thenSleepWhile(BooleanSupplier condition, long milliseconds) {
            return thenSleepWhile(condition, Duration.ofMillis(milliseconds));
        }

        public Builder thenSleepWhile(BooleanSupplier condition, Duration maxDuration) {
            return addSubOp(Operation.newBuilder()
                    .addPreReq(condition)
                    .addSubOp(() -> Execution.delay(50, 55))
                    .maximumDuration(maxDuration)
                    .untilPreconditionsFail()
                    .build());
            // .andThen(Activities.stopScriptIf(condition)));
        }

        public Builder thenSleepUntil(BooleanSupplier condition, Duration maxDuration) {
            return thenSleepWhile(condition, maxDuration); // TODO: Fix this. should be inverted.
//            return thenSleepWhile(Predicates.not(condition.), maxDuration);
        }

        /**
         * Another name for `thenSleepUntil`.
         */
        public Builder sleepUntil(BooleanSupplier condition) {
            return thenSleepUntil(condition);
        }

        /**
         * Waits for up to 2 minutes for a condition to become true, checking every ~50 ms. If the condition is not
         * true after 2 minutes, the program will termin8.
         */
        public Builder thenSleepUntil(BooleanSupplier condition) {
            return thenSleepUntil(condition, Duration.ofMinutes(2));
        }

        /**
         * Another name for `thenPauseFor`
         */
        public Builder pauseFor(Duration duration) {
            return thenPauseFor(duration);
        }

        /**
         * Sleeps for a duration, roughly. To keep Jagex on its toes, the actual duration waited for is randomly between
         * the input duration and 20% longer than that duration.
         */
        public Builder thenPauseFor(Duration duration) {
            return addSubOp(Operation.newBuilder()
                    .addSubOp(() -> Execution.delay(duration.toMillis(), duration.toMillis() * 120 / 100))
                    .build());
        }

        /**
         * Continues to repeat this activity until some other precondition fails.
         */
        public Builder untilPreconditionsFail() {
            return maximumTimes(Integer.MAX_VALUE);
        }

        /**
         * Ensures an activity is only run once.
         * <p>
         * By default, activities run until a precondition fails.
         */
        public Builder onlyOnce() {
            return maximumTimes(1);
        }
    }
}
