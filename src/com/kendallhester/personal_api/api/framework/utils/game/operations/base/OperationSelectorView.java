package com.kendallhester.personal_api.api.framework.utils.game.operations.base;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

public class OperationSelectorView extends JFrame {
    OperationSelectorModel model;
    JPanel activityConfigView;

    public OperationSelectorView(OperationSelectorModel model) {
        super("Activity Selector");

        this.model = model;
        this.activityConfigView = new JPanel();

        setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

        add(createActivitySelector());
        add(activityConfigView);

        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                Globals.script.stop("Closing selector view instance.");
            }
        });
        pack();
    }

    private JComboBox createActivitySelector() {
        JComboBox comboBox = new JComboBox(prependDefaultOption(model.getActivityKeys()));
        comboBox.addActionListener(event -> {
            model.setOperationByName((String) comboBox.getSelectedItem());
            updateViewForSelectedActivity();
            revalidate();
            pack();
        });
        return comboBox;
    }

    // TODO(dmattia): Finish adding "None Selected" option to dropdown
    private static String[] prependDefaultOption(String[] options) {
        ArrayList<String> list = new ArrayList<>(Arrays.asList(options));
        list.add(0, "None Selected");
        return list.stream().toArray(String[]::new);
    }

    private JComponent getComponentForConfigOptions(OperationConfigModel configModel) {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        for (OperationConfigModel.TextOption textOption : configModel.textOptions) {
            JTextField field = new JTextField(textOption.value);

            field.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    model.getOperation().flatMap(Operation::getConfigModel).ifPresent(config -> {
                        config.keyValStore.put(textOption.key, field.getText());
                    });
                }
            });
            panel.add(field);
        }

        return panel;
    }

    private void updateViewForSelectedActivity() {
        activityConfigView.removeAll();

        model.getOperation()
                .flatMap(Operation::getConfigModel)
                .map(this::getComponentForConfigOptions)
                .ifPresent(activityConfigView::add);
    }
}
