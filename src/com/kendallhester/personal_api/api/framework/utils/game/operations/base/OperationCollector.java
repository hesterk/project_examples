package com.kendallhester.personal_api.api.framework.utils.game.operations.base;

import java.util.Collections;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class OperationCollector implements Collector<Operation, Operation.Builder, Operation.Builder> {
    @Override
    public Supplier<Operation.Builder> supplier() {
        return Operation::newBuilder;
    }

    @Override
    public BiConsumer<Operation.Builder, Operation> accumulator() {
        return Operation.Builder::addSubOp;
    }

    @Override
    public BinaryOperator<Operation.Builder> combiner() {
        return (first, second) -> first.addSubOp(second.build());
    }

    @Override
    public Function<Operation.Builder, Operation.Builder> finisher() {
        return builder -> builder;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Collections.emptySet();
    }
}
