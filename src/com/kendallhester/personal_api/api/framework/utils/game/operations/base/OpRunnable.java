package com.kendallhester.personal_api.api.framework.utils.game.operations.base;

import com.gameclient.game.api.script.framework.LoopingBot;

public abstract class OpRunnable extends LoopingBot {

    OperationSelectorModel model;
    OperationSelectorView view;

    public abstract OperationSelectorModel getSelectionModel();

    public OpRunnable() {
        Globals.script = this;
        setLoopDelay(220, 350);
    }

    @Override
    public void onStart(String... args) {
        model = getSelectionModel();
        view = new OperationSelectorView(model);
        view.setVisible(true);
        setLoopDelay(220, 350);
    }

    @Override
    public void onStop() {
        super.onStop();
        view.dispose();
    }

    @Override
    public void onLoop() {
        model.getOperation().ifPresent(Operation::run);
    }
}
