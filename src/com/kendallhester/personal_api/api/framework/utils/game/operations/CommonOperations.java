package com.kendallhester.personal_api.api.framework.utils.game.operations;

import com.kendallhester.personal_api.api.framework.utils.game.operations.base.Operation;
import com.kendallhester.personal_api.api.framework.utils.misc.Sorting;
import com.kendallhester.personal_api.api.game.player.Actions;
import com.gameclient.game.api.hybrid.local.hud.interfaces.Inventory;


public class CommonOperations {

    public static boolean dropAll(String... x) {
        Operation op = Operation.newBuilder()
                .addPreReq(() -> Inventory.containsAnyOf(x))
                .addSubOp(() -> Actions.dropAll(true, Sorting.getSnakeSort(), x))
                .build();
        op.run();

        return !Inventory.containsAnyOf(x);
    }

}
