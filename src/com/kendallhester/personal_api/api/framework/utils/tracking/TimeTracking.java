package com.kendallhester.personal_api.api.framework.utils.tracking;

import com.kendallhester.personal_api.api.framework.utils.formatting.StringUtil;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class TimeTracking {

    private static final String dateFormat = "EEE, dd MMM yyyy HH:mm:ss z";
        private static final String defaultServer = "https://www.google.com";

        private static String getServerHttpDate(String serverUrl) throws IOException {
            URL url = new URL(serverUrl);
            URLConnection connection = url.openConnection();

            Map<String, List<String>> httpHeaders = connection.getHeaderFields();

            for (Map.Entry<String, List<String>> entry : httpHeaders.entrySet()) {
                String headerName = entry.getKey();
                if (headerName != null && headerName.equalsIgnoreCase("date")) {
                    return entry.getValue().get(0);
                }
            }
            return null;
        }

        public static Date getCurrentTime() {
            return getCurrentGoogleTime();
        } // defaulting to Google right now..

        public static Date getCurrentGoogleTime() {
            return getCurrentTime(defaultServer, false);
        }

        public static Date getCurrentTime(String server) {
            return getCurrentTime(server, true);
        }

        // this is used when I want to see that I'm failing to connect to the server
        public static Date getCurrentTimeStrictly(String server) { return getCurrentTime(server, false); }

        public static Date getCurrentTime(String server, boolean fallback) {
            if (StringUtil.isNullOrEmpty(server)) {
                if (fallback)
                    return getCurrentGoogleTime();
                return new Date();
            } else
                return getCurrentTime(server, dateFormat, fallback);
//                return getCurrentTime(server, "EEE, dd MMM yyyy HH:mm:ss z", fallback);
        }

        public static Date getCurrentTime(String server, String format, boolean fb) {
            if (!StringUtil.exists(server)) {
                if (fb)
                    return getCurrentGoogleTime();
                return new Date();
            } else if (!StringUtil.exists(format))
                return getCurrentTime(server, fb);
            else {
                Date i = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
                try {
                    i = sdf.parse(getServerHttpDate(server));
                } catch (ParseException | IOException e) {
                    e.printStackTrace();
                }
                return i;
            }

        }

        public static String getCurrentTimeAsString() {
            return getCurrentTimeAsString(defaultServer); // default to google right now.
        }

        public static String getCurrentTimeAsString(String server) {
            Date current = StringUtil.equals(server, defaultServer) ? getCurrentTime() : getCurrentTime(server);
            if (current != null) {
                DateFormat df = new SimpleDateFormat(dateFormat);
                return df.format(current);
            }
            return "";
        }

        public static LocalDateTime getCurrentTimeAsLDT() {
            return getAsLocalDateTime(getCurrentTime());
        }

        public static LocalDateTime getGoogleTimeAsLDT() {
            return getAsLocalDateTime(getCurrentGoogleTime());
        }

        public static LocalDateTime getCurrentTimeAsLDT(String server) {
            return getAsLocalDateTime(getCurrentTime(server));
        }

        public static LocalDateTime getAsLocalDateTime(Date x) {
            if (x != null) {
                return x.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime();
            }
            return LocalDateTime.MAX; // error
        }
}
