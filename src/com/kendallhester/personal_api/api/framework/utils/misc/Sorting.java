package com.kendallhester.personal_api.api.framework.utils.misc;

import com.gameclient.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.gameclient.game.api.hybrid.location.Coordinate;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Sorting {

    private static final List<Integer> snake = List.of(0,1,2,3,7,6,5,4,8,9,10,11,15,14,13,12,16,17,18,19,23,22,21,20,24,25,26,27);
    private static final List<Integer> revSnake = List.of(3,2,1,0,4,5,6,7,11,10,9,8,12,13,14,15,19,18,17,16,20,21,22,23,27,26,25,24);
    private static final List<Integer> downUp = List.of(0,4,8,12,16,20,24,1,5,9,13,17,21,25,2,6,10,14,18,22,26,3,7,11,15,19,23,27);
    private static final List<Integer> downAcross = List.of(0,4,8,12,16,20,24,25,21,17,13,9,5,1,2,6,10,14,18,22,26,27,23,19,15,11,7,3);
    private static final List<Integer> middleSort = List.of(13,14,12,15,9,10,17,18,8,11,16,19,0,1,2,3,4,5,6,7,20,21,22,23,24,25,26,27);

    public static Comparator<SpriteItem> getSnakeSort() { return getInvSort(snake); }
    public static Comparator<SpriteItem> getReverseSnakeSort() { return getInvSort(revSnake); }
    public static Comparator<SpriteItem> getVerticalSort() { return getInvSort(downUp); }
    public static Comparator<SpriteItem> getVerticalFlexSort() { return getInvSort(downAcross); }
    public static Comparator<SpriteItem> getMiddleSort() { return getInvSort(middleSort); }

    public static Comparator<SpriteItem> getOppositeSort(int num) {
        if (num % 2 == 0)
            return getInvSort(num-1);
        return getInvSort(num+1);
    }

    public static Comparator<SpriteItem> getInvSort(int num) {
        List<Integer> sorter = null;
        switch (num) {
            case 1: sorter = snake; break;
            case 2: sorter = revSnake; break;
            case 3: sorter = downUp; break;
            case 4: sorter = downAcross; break;
        }
        final List<Integer> chosen = sorter;
        return getInvSort(chosen);
    }

    public static Comparator<SpriteItem> getAlphabeticalSort() {
        return Comparator.comparing(a -> Objects.requireNonNull(a.getDefinition()).getName());
    }

    public static Comparator<SpriteItem> getReverseAlphabetical() {
        return (a, b) -> Objects.requireNonNull(b.getDefinition()).getName().compareTo(Objects.requireNonNull(a.getDefinition()).getName());
    }

    private static Comparator<SpriteItem> getInvSort(List<Integer> list) {
        if (list != null && !list.isEmpty())
            return Comparator.comparingInt(a -> list.indexOf(a.getIndex()));
        return Comparator.comparingInt(SpriteItem::getIndex);
    }

    public static Comparator<Coordinate> getNearestReach(Coordinate x){
        return Comparator.comparingDouble(a -> a.distanceTo(x));
    }

    public static Comparator<Coordinate> getFurthestReach(Coordinate x){
        return (a,b) -> Double.compare(b.distanceTo(x), a.distanceTo(x));
    }

}
