package com.kendallhester.personal_api.api.framework.utils.misc;

import com.kendallhester.personal_api.api.framework.bots.looping.APILoopingStructure;
import com.gameclient.game.api.hybrid.Environment;
import com.gameclient.game.api.script.Execution;

import java.util.Objects;
import java.util.concurrent.Callable;

public class Process {

    public static boolean delay(String status, int min, int max) {
        return delay(null, status, min, max);
    }

    public static boolean delay(Callable<Boolean> interact, String status, int min) {
        return delay(interact, status, min, min+1); // i'm lazy.
    }

    public static boolean delay(Callable<Boolean> interact, int min, int max) {
        return delay(interact, null, min, max);
    }

    public static boolean delay(Callable<Boolean> interact, String status, int min, int max) {
        try {
            boolean result = false;
            if (interact != null) {
                result = interact.call();
            }
            if (interact == null || result) {
                if (status != null)
                    ((APILoopingStructure) Objects.requireNonNull(Environment.getBot())).setStatus(status);
                return Execution.delay(min, max);
            }
        } catch (Exception e) {
            APILoopingStructure tB = (APILoopingStructure) Environment.getBot();
            if (tB != null) tB.setStatus("Couldn't process delay.");
        }
        return false;
    }

    private static boolean delayInteract(boolean until, Callable<Boolean> interact, Callable<Boolean> timeout, String status, int min, int max) throws Exception {
        boolean result = interact != null ? interact.call() : true;
        if (result) {
            if (status != null)
                ((APILoopingStructure) Objects.requireNonNull(Environment.getBot())).setStatus(status);
            if (until) {
                Execution.delayUntil(timeout, min, max);
            } else {
                Execution.delayWhile(timeout, min, max);
            }
            return timeout.call();
        }
        return false;
    }

    public static boolean delayWhile(Callable<Boolean> interact, Callable<Boolean> timeout) {
        return delayWhile(interact, timeout, 2147483646, 2147483647);
    }

    public static boolean delayWhile(Callable<Boolean> interact, Callable<Boolean> timeout, int min) {
        return delayWhile(interact, timeout, min, min+1);
    }

    public static boolean delayWhile(Callable<Boolean> interact, Callable<Boolean> timeout, String status, int min) {
        return delayWhile(interact, timeout, status, min, min+1);
    }

    public static boolean delayWhile(Callable<Boolean> interact, Callable<Boolean> timeout, int min, int max) {
        return delayWhile(interact, timeout, null, min, max);
    }

    public static boolean delayWhile(Callable<Boolean> interact, Callable<Boolean> timeout, String status, int min, int max) {
        try {
            return delayInteract(false, interact, timeout, status, min, max);
        } catch (Exception ignored) {
            APILoopingStructure tB = (APILoopingStructure) Environment.getBot();
            if (tB != null) tB.setStatus("Couldn't process delayWhile.");
        }
        return false;
    }

    public static boolean delayUntil(Callable<Boolean> interact, Callable<Boolean> timeout) {
        return delayUntil(interact, timeout, 2147483646, 2147483647);
    }

    public static boolean delayUntil(Callable<Boolean> interact, Callable<Boolean> timeout, int min) {
        return delayUntil(interact, timeout, null, min);
    }

    public static boolean delayUntil(Callable<Boolean> interact, Callable<Boolean> timeout, String status, int min) {
        return delayUntil(interact, timeout, status, min, min+1);
    }

    public static boolean delayUntil(Callable<Boolean> interact, Callable<Boolean> timeout, int min, int max) {
        return delayUntil( interact, timeout, null, min, max);
    }

    public static boolean delayUntil(Callable<Boolean> interact, Callable<Boolean> timeout, String status, int min, int max) {
        try {
            return delayInteract(true, interact, timeout, status, min, max);
        } catch (Exception ignored) {
            APILoopingStructure tB = (APILoopingStructure) Environment.getBot();
            if (tB != null) tB.setStatus("Couldn't process delayUntil.");
        }
        return false;
    }
}
