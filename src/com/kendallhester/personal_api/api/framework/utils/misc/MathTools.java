package com.kendallhester.personal_api.api.framework.utils.misc;

import java.util.Random;

public class MathTools {

    public static double round(double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    public static boolean randBoolean() {
        return randInt(1, 10) > 5;
    }

    public static long randLong(long one, long two) {
        return (one + (long) (Math.random() * ((two - one) + 1)));
    }

    public static long randLong(double one, double two) { return randLong((long) one, (long) two); }

    public static int randInt(int one, int two) {
        return (one + (int) (Math.random() * ((two - one) + 1)));
    }

    public static double randDouble(double one, double two) {
        Random r = new Random();
        return one + (two - one) * r.nextDouble();
    }

    /**
     * @param value - original value
     * @param variance - maximum variance value allowed. i.e. 0.8 allows anywhere between 0 - 0.8 from original double
     * @return Returns a double with a deviation downards (subtraction) from the original value. This
     * can be anything from 0 (no deviation) to the passed variance value (the maximum deviation allotted).
     */
    public static double getDeviationDownwards(double value, double variance) {
        return value - MathTools.randDouble(0, variance);
    }

    /**
     * @param value - original value
     * @param variance - maximum variance value allowed. i.e. 0.8 allows anywhere between 0 - 0.8 from original double
     * @return Returns a double with a deviation upwards (addition) from the original value. This
     * can be anything from 0 (no deviation) to the passed variance value (the maximum deviation allotted).
     */
    public static double getDeviationUpwards(double value, double variance) {
        return value + MathTools.randDouble(0, variance);
    }

    /**
     * @param value - original value
     * @param variance - maximum variance value allowed. i.e. 0.8 allows anywhere between 0 - 0.8 from original double
     * @return Returns a double with a deviation in a random direction away from the original value. This
     * can be anything from 0 (no deviation) to the passed variance value (the maximum deviation allotted).
     */
    public static double getRandomDeviation(double value, double variance) {
        int direction = randInt(0, 1);
        return direction < 1
                ? getDeviationDownwards(value, variance)
                : getDeviationUpwards(value, variance);
    }

    public static int getRandomDeviation(int value, int variance) {
        return (int) getRandomDeviation((double) value, (double) variance);
    }
}
