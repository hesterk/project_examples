package com.kendallhester.personal_api.api.framework.bots.looping;

import com.kendallhester.bots.tree_structures.mining_example.main.Mining;

public enum BotType {

    MINING("Mining Example Bot", Mining.class);

    private final String name;
    private final Class<? extends APILoopingStructure> type;

    BotType(String readableName, Class<? extends APILoopingStructure> classType) {
        this.name = readableName;
        this.type = classType;
    }

    public Class<? extends APILoopingStructure> getType() { return type; }

    public String getManifestName() {
        return name; // names of bots change, but the BotType will always remain.
    }
}
