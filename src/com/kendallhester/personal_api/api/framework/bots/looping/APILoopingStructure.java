package com.kendallhester.personal_api.api.framework.bots.looping;

import com.kendallhester.personal_api.api.framework.bots.universal.resources.CustomPlayerVariation;
import com.kendallhester.personal_api.api.framework.bots.universal.resources.Users;
import com.kendallhester.personal_api.api.framework.bots.universal.tracking.APIListener;
import com.kendallhester.personal_api.api.framework.bots.universal.tracking.inventory.APIItemListener;
import com.kendallhester.personal_api.api.framework.bots.universal.tracking.skills.APISkillListener;
import com.kendallhester.personal_api.api.framework.breaks.BreakHandler;
import com.kendallhester.personal_api.api.framework.utils.game.OSCamera;
import com.kendallhester.personal_api.api.framework.utils.game.Game;
import com.kendallhester.personal_api.api.framework.utils.misc.MathTools;
import com.kendallhester.personal_api.api.framework.utils.misc.Sorting;
import com.kendallhester.personal_api.api.framework.utils.misc.Process;
import com.kendallhester.personal_api.api.framework.utils.tracking.TimeTracking;
import com.gameclient.game.api.client.ClientUI;
import com.gameclient.game.api.client.embeddable.EmbeddableUI;
import com.gameclient.game.api.hybrid.Environment;
import com.gameclient.game.api.hybrid.GameEvents;
import com.gameclient.game.api.hybrid.entities.definitions.ItemDefinition;
import com.gameclient.game.api.hybrid.input.Mouse;
import com.gameclient.game.api.hybrid.local.Skill;
import com.gameclient.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.gameclient.game.api.hybrid.util.StopWatch;
import com.gameclient.game.api.hybrid.util.collections.Pair;
import com.gameclient.game.api.script.Execution;
import com.gameclient.game.api.script.framework.LoopingBot;
import com.gameclient.game.api.script.framework.listeners.ChatboxListener;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.List;
import java.util.Queue;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.gameclient.game.api.hybrid.RuneScape.isLoggedIn;

public abstract class APILoopingStructure extends LoopingBot implements EmbeddableUI, ChatboxListener {

    protected SimpleObjectProperty<Node> botInterfaceProperty;

    private boolean guiWait = true, dev = false;
    private long afkPrecursor, afkDuration, turnPrecursor, lastAfkEvent, lastTurnEvent;
    private double afkProbability, turnProbability, productivityRate;
    private String status = "Loading...", lastStatus = status;
    private Pair<Comparator<SpriteItem>, Double> sortingPair;
    private Comparator<SpriteItem> oppositeSort;

    private final Queue<String> logQueue = new LinkedList<>();
    private final Map<String, Boolean> booleanMap = new HashMap<>();
    private final BreakHandler breakHandler = new BreakHandler(this, null, 0);
    private final List<APIListener> trackers = new ArrayList<>();
    private final StopWatch runtime = new StopWatch(), onlineTimer = new StopWatch();
    private final APISkillListener skillListener;
    private final APIItemListener itemListener;

    public APILoopingStructure() {
        skillListener = new APISkillListener(this);
        itemListener = new APIItemListener(this);
        addTrackers(skillListener, itemListener);
        getEventDispatcher().addListener(this);
        setEmbeddableUI(this);
    }

    public final void onStart(String... args) {
        Mouse.setPathGenerator(Mouse.MLP_PATH_GENERATOR);
        CustomPlayerVariation.initializeKeys();
        addTrackers();

        assignPlayerVariation();
        botSpecificStartup();
        runtime.start();
        setStatus("Loading..");
        setStatus("" + getMetaData().getName() + " v" + getMetaData().getVersion() + " started.");
        setStatus("New session: " + TimeTracking.getCurrentTimeAsString(), 1);

        addDefaultBooleans();
        addBooleans(); // should be renamed to create config

        // the following will only check & set if the ui hasn't loaded by now.
        isDev();
        isPrem();
        isBeta();

        if (guiWait) setStatus("Waiting until user starts execution.");
    }

    public final void onStop() {
        Process.delay("Processing summary", 400, 550);
        setStatus("Total time: " + runtime.getRuntimeAsString() + " (" + onlineTimer.getRuntimeAsString() + " spent active)");
        botSpecificStop();
        printTrackerResults();
        setStatus(String.format("Thanks for using %s. If you enjoyed it, consider dropping a review!", getMetaData().getName()));
        System.gc();
    }

    public final void onResume() {
        onlineTimer.start();
        trackers.forEach(APIListener::resumeTrackers);
        botSpecificResume();
        setStatus("Execution resumed.", 1);
    }
    public final void onPause() {
        onlineTimer.stop();
        trackers.forEach(APIListener::pauseTrackers);
        botSpecificPause();
        setStatus("Execution stopped.", 1);
    }
    public final void end() {
        Execution.delay(200,400); // let other statuses set.
        end(false);
    }
    public final void end(boolean writeOldStatus) {
        if (isRunning()) {
            if (writeOldStatus) stop(status);
            else if (isRunning()) stop("Turning off.");
        }
    }
    public final void endSafely() {
        Process.delayWhile(Game::logout, Game::isLoggedIn, "Closing out your character.", 2000, 3000);
        end(false);
    }

    private void assignPlayerVariation() {
        try {
            setPlayerVariation(CustomPlayerVariation.Key.TIME_BEFORE_AFK.getAsLong(),
                    CustomPlayerVariation.Key.CHANCE_OF_AFK.getAsDouble(),
                    CustomPlayerVariation.Key.LENGTH_OF_AFK.getAsLong(),
                    CustomPlayerVariation.Key.TENDENCY_TO_RANDOMLY_TURN.getAsDouble(),
                    CustomPlayerVariation.Key.TIME_BEFORE_TURNING.getAsLong(),
                    CustomPlayerVariation.Key.PRIMARY_SORTING_TYPE.getAsInteger(),
                    CustomPlayerVariation.Key.PRIMARY_SORTING_WEIGHT.getAsDouble(),
                    CustomPlayerVariation.Key.TENDENCY_TO_BE_PRODUCTIVE.getAsDouble());
        }
        catch (Exception e)
        { // should be NPE, but have seen a few others
            setStatus("Assigning default values.");
            setPlayerVariation(MathTools.randLong(700000, 11000000),
                    MathTools.randDouble(.02, .09),
                    MathTools.randLong(7000, 22000),
                    MathTools.randDouble(.01, .19),
                    MathTools.randLong(140000, 321928),
                    MathTools.randInt(1, 6),
                    MathTools.randDouble(.49, .82),
                    MathTools.randDouble(.21, .72));
        }
    }

    private void setPlayerVariation(long preAfkTime, double afkChance, long afkLength, double turnChance,
                                long preTurn, int sortType, double sortChance, double productivity)
    {
        afkPrecursor = preAfkTime;
        afkProbability = afkChance;
        afkDuration = afkLength;
        turnProbability = turnChance;
        turnPrecursor = preTurn;
        sortingPair = new Pair<>(Sorting.getInvSort(sortType), sortChance);
        oppositeSort = Sorting.getOppositeSort(sortType);
        productivityRate = productivity;
    }

    /*
     * Trackers
     */
    public final APISkillListener getSkillListener() {
        return skillListener;
    }
    public final APIItemListener getItemListener() {
        return itemListener;
    }
    private void addTrackers(APIListener... listeners) {
        trackers.addAll(List.of(listeners));
    }
    private void printTrackerResults() {
        if (itemListener.getSize() > 0 && itemListener.getTotalGoldChanges() != 0)
            itemListener.printTotalGoldChanges();
        if (skillListener.getSize() > 1) skillListener.printTotalExperienceGain();
        skillListener.printStats();
    }

    /*
     * Abstract methods
     */
    protected abstract void botSpecificStartup(); // set startups, assignPlayerVariation, etc.
    protected abstract void botSpecificStop(); // output bot specific status messages and such
    protected abstract void addBooleans();
    public abstract BotType getBotType();

    /*
     * Non abstract; some override-able, some final
     */
    public void botSpecificPause(){}
    public void botSpecificResume(){}
    public void botSpecificSkillEventHandling(Skill sk, int change, boolean xpEvent){}
    public void botSpecificItemEventHandling(ItemDefinition e, int change, boolean added){}

    public final boolean checkTurn() {
        if (getOnlineRuntime() - getLastEvent(false) > getEventPrecursor(false)
                && MathTools.randDouble(0, 1) < getEventProbability(false)) {
            turn();
            return true;
        }
        return false;
    }
    private void turn() {
        Process.delay(OSCamera::turnCamera, "[Variation] Turning.", 0, 1);
        setLastEvent(getOnlineRuntime(), false);
    }

    /*
     * Config/BooleanMap methods
     */
    private void addDefaultBooleans() {
        addFalseConfigs("premium", "beta");
    }
    protected final void addFalseConfigs(String... values) {
        Arrays.stream(values).forEach(x -> booleanMap.putIfAbsent(x, false));
    }
    public final void setConfig(String key, boolean value) {
        booleanMap.put(key, value);
    }
    public final boolean getConfig(String key) {
        if (booleanMap.containsKey(key))
            return booleanMap.get(key);
        return false;
    }

    /*
     * Common getters
     */
    private boolean checkPrem() {
        if (getMetaData().getHourlyPrice().doubleValue() > 0 || Users.checkIfFreePremium(getBotType(), Environment.getForumName())) {
            setStatus("Enabling purchased features.");
            setConfig("premium", true);
        }
        return getConfig("premium");
    }
    private boolean checkBeta() {
        if (getMetaData().getName().contains("Beta") && Users.checkIfBetaTesting(getBotType(), Environment.getForumName()))
        {
            setStatus("Enabling beta access."); // beta is allowed w premium features enabled
            setConfig("beta", true);
        }
        return getConfig("beta");
    }
    private boolean checkDev() {
        if (Environment.getForumName().equals("Kendall")) {
            setStatus("Enabling developer features.");
            dev = true;
        }
        return dev;
    }

    public final boolean isPrem() { return getConfig("premium") || checkPrem(); }
    public final boolean isBeta() { return getConfig("beta") || checkBeta(); }
    public final boolean isDev() { return dev || checkDev() ; }
    public final long getLastEvent(boolean afk) { return afk ? lastAfkEvent : lastTurnEvent; }
    public final long getEventPrecursor(boolean afk) { return afk ? afkPrecursor : turnPrecursor; }
    public final double getEventProbability(boolean afk) { return afk ? afkProbability : turnProbability; }
    public final void setLastEvent(long val, boolean afk) {
        if (afk) lastAfkEvent = val;
        else lastTurnEvent = val;
    }
    public final Comparator<SpriteItem> getOppositeSort() { return oppositeSort;}
    public final Pair<Comparator<SpriteItem>, Double> getSortingPair() { return sortingPair; }
    public final void toggleDev() { if (dev || Environment.getForumName().equalsIgnoreCase("Kendall")) dev = !dev; }


    /*
     * Logging and status methods
     */
    public final boolean logDev(String s) {
        if (dev) return setStatus("[developer] " + s, 1);
        return false;
    }
    public final boolean setStatus(String input, Object... i) {
        return setStatus(String.format(input, i));
    }
    public final boolean setStatus(String input) {
        return setStatus(input, 0);
    }
    public final boolean setStatus(String input, int level) {
        if (input != null) {
            status = input;
            if (!status.equals(lastStatus) || dev) {
                log(level);
                if (logQueue.size() >= 150)
                    logQueue.poll();
                    //logQueue.remove(logQueue.peek());
                logQueue.add(getRuntimeString() + " -   " + input);
                lastStatus = input;
            }
        }
        return true;
    }
    protected final void log(int type) {
        switch (type) {
            case 1: getLogger().debug(status); break;
            case 2: getLogger().warn(status); break;
            case 3: getLogger().severe(status); break;
            default:
            case 0: getLogger().info(status); break;
        }
    }
    public final List<String> getLogQueueAsList() {
        if (logQueue.size() != 0)
            return (List) logQueue;
        return new ArrayList<>();
    }
    public final String[] getLogQueue() {
        if (logQueue.size() != 0)
            return logQueue.toArray(new String[0]);
        return new String[]{""};
    }

    /*
     * Common UI value methods
     */
    public final void startOnlineTimer() { onlineTimer.start(); }
    public final long getRuntimeAs(TimeUnit unit) { return runtime.getRuntime(unit);}
    public final long getOnlineRuntime() { return onlineTimer.getRuntime();}
    public final String getRuntimeString() { return runtime.getRuntimeAsString(); }
    public final String getStatus() { return status; }
    public final boolean isWaiting() { return guiWait; }
    public final void setWait(boolean val) { guiWait = val; }

    /*
     * Breaking
     */
    public final BreakHandler getBreaker() { return breakHandler; }

    public final void saveBreaks() {
        String result = getBreaker().saveBreaks(this) 
            ? "Your breaks have been saved." 
            : "You don't have any valid breaks to save.";
            
        ClientUI.showAlert(result);
    }

    public final void loadBreaks() {
        String result = getBreaker().loadBreaks(this) 
            ? "Your breaks have been loaded."
            : "You don't have any valid breaks to load.";
        
        ClientUI.showAlert(result);
    }
}
