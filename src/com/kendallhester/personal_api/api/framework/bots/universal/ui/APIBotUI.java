package com.kendallhester.personal_api.api.framework.bots.universal.ui;

import com.kendallhester.personal_api.api.framework.bots.looping.APILoopingStructure;
import com.kendallhester.personal_api.api.framework.bots.universal.tracking.inventory.ItemTracker;
import com.kendallhester.personal_api.api.framework.bots.universal.tracking.skills.APISkillListener;
import com.kendallhester.personal_api.api.framework.bots.universal.tracking.skills.SkillTracker;
import com.kendallhester.personal_api.api.framework.breaks.Break;
import com.kendallhester.personal_api.api.framework.breaks.BreakHandler;
import com.kendallhester.personal_api.api.framework.utils.formatting.StringUtil;
import com.kendallhester.personal_api.api.framework.utils.formatting.UniversalFormat;
import com.kendallhester.personal_api.api.framework.utils.game.Game;
import com.kendallhester.personal_api.api.framework.utils.misc.MathTools;
import com.gameclient.game.api.hybrid.GameEvents;
import com.gameclient.game.api.hybrid.local.Skill;
import com.gameclient.game.api.hybrid.util.Resources;
import com.gameclient.game.api.hybrid.util.Time;
import com.gameclient.game.api.hybrid.util.collections.Pair;
import com.gameclient.game.api.hybrid.util.collections.PairList;
import com.gameclient.game.api.script.framework.core.LoopingThread;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public abstract class APIBotUI extends GridPane implements Initializable { // controller

    /**
     * Universal elements
     */
    @FXML private Button controls_BT, skillHandler_BT, itemHandler_BT, breakHandler_BT, logs_BT;
    @FXML private Label status_LL, runtime_LL, title_LL, version_LL;

    /**
     * Controls page elements
     */
    @FXML private VBox control_VB;
    @FXML protected Button start_BT;

    private String runtimeString="00:00:00", status="Loading";
    private String[] logs = new String[]{""};

    /**
     * SkillTracker page elements
     */

    @FXML protected Label customTracker1_LL, customTracker2_LL, focusedSkill_LL;
    @FXML private VBox skillTracker_VB;
    @FXML private Label exp_LL, levels_LL, expPerHour_LL, timeTilLevel_LL, currentExp_LL;
    @FXML private PieChart skillsChart_PC;

    private boolean needToUpdateChart;
    private APISkillListener skillListener;
    private final Map<Skill, Integer> skillLevelMap = new LinkedHashMap<>();
    private Skill targetSkill;
    private ObservableList<PieChart.Data> chartList = FXCollections.observableArrayList();
    private final Map<Skill, PieChart.Data> chartMap = new HashMap<>();

    /**
     * ItemTracker page elements
     */
    @FXML private VBox item_VB;
    @FXML private Button resetItem_BT, resetAllItems_BT;
    @FXML private TableView<ItemTracker> items_TV;
    @FXML private TableColumn<ItemTracker, String> isNoted_TC, goldChange_TC;
    @FXML private TableColumn<ItemTracker, String> itemName_TC, itemTime_TC;
    @FXML private TableColumn<ItemTracker, String> itemAmount_TC, itemPrice_TC;
    @FXML private Label profit_LL;


    /**
     * Logs page elements
     */
    @FXML private ListView<String> statusList_LV;
    @FXML private Button devToggle_BT;
    @FXML private VBox logs_VB;

    /**
     * BreakHandler variables
     */
    @FXML private GridPane break_GP;
    @FXML private RadioButton customBreak_RB, randomBreak_RB;
    @FXML private HBox randomBreaks_HB, customBreak_HB;
    @FXML private TableView<Break> breaks_TV;
    @FXML private TableColumn<Break, String> breakNumber_TC, startingTime_TC, endingTime_TC, duration_TC;

    @FXML private Label max_play_LL, max_break_LL, min_play_LL, min_break_LL;
    @FXML private Spinner<Integer> breakAmount_SP;
    @FXML private TextField start_TF, end_TF;
    @FXML private Button create_BT, createRandomBreaks_BT, deleteBreak_BT, saveBreak_BT, loadBreak_BT, append_BT, clear_BT;
    @FXML private Slider min_play_SL, min_break_SL, max_break_SL, max_play_SL;

    private final ObservableList<Break> observableBreakList = FXCollections.observableArrayList();
    private final ObservableList<ItemTracker> observableItemList = FXCollections.observableArrayList();
    private final ToggleGroup breakType = new ToggleGroup();

    private final Set<Pane> panes = new HashSet<>();
    private final APILoopingStructure bot;
    private boolean setHoverProperty;

    protected long online = 0;
    protected String profit = "+0 (0gp/hr)";

    /**
     * Constructor
     */
    public APIBotUI(APILoopingStructure bot) {
        this.bot = bot;
        FXMLLoader loader = new FXMLLoader();
        Future<InputStream> stream = bot.getPlatform().invokeLater(() -> Resources.getAsStream(findResourceStream()));
        loader.setController(this);
        loader.setRoot(this);

        try { loader.load(stream.get()); }
        catch (IOException | InterruptedException | ExecutionException e) {
            bot.setStatus("Error loading the ui.", 3);
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public <T extends APILoopingStructure> T getBotObject() {
        assert bot != null;
        return (T)getBotObject(bot.getBotType().getType());
    }

    private <T extends APILoopingStructure> T getBotObject(Class<T> type) {
        assert bot != null;
        return type.cast(bot);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        GameEvents.Universal.LOGIN_HANDLER.disable();
        GameEvents.Universal.LOBBY_HANDLER.disable();

        bot.setStatus("Initializing ui..");
        skillListener = getBot().getSkillListener();
        skillsChart_PC.setStyle("-fx-background-color: rgba(40, 40, 40, 255); -fx-background-radius: 10;");
        start_BT.setDisable(true);

        //break section
        breakNumber_TC.setCellValueFactory(new PropertyValueFactory<>("breakNumber"));
        startingTime_TC.setCellValueFactory(new PropertyValueFactory<>("startAsString"));
        endingTime_TC.setCellValueFactory(new PropertyValueFactory<>("endAsString"));
        duration_TC.setCellValueFactory(new PropertyValueFactory<>("durationAsString"));
        breakAmount_SP.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 12, 3, 1));
        breaks_TV.setItems(observableBreakList);
        randomBreak_RB.setToggleGroup(breakType);
        customBreak_RB.setToggleGroup(breakType);

        createRandomBreaks_BT.setOnAction(createRandBreaks());
        create_BT.setOnAction(createSpecificBreak(false));
        append_BT.setOnAction(createSpecificBreak(true));
        deleteBreak_BT.setOnAction(removeBreak());
        saveBreak_BT.setOnAction(saveBreaks());
        loadBreak_BT.setOnAction(loadBreaks());
        clear_BT.setOnAction(clearFields());
        //end break section

        // items section
        itemAmount_TC.setCellValueFactory(new PropertyValueFactory<>("amountAsString"));
        itemName_TC.setCellValueFactory(new PropertyValueFactory<>("name"));
        itemPrice_TC.setCellValueFactory(new PropertyValueFactory<>("priceAsString"));
        isNoted_TC.setCellValueFactory(new PropertyValueFactory<>("isNotedAsString"));
        goldChange_TC.setCellValueFactory(new PropertyValueFactory<>("goldChangeAsString"));
        itemTime_TC.setCellValueFactory(it -> new SimpleStringProperty(it.getValue().getTimeAsString()));
        items_TV.setItems(observableItemList);
        resetItem_BT.setOnAction(resetItem());
        resetAllItems_BT.setOnAction(resetAllItems());
        // end items section

        if (!getBot().isDev()) {
            devToggle_BT.setDisable(true);
            devToggle_BT.setVisible(false);
        } else {
            devToggle_BT.setOnAction(toggleDev());
        }

        setVisible(true);
        version_LL.setText("Author: Kendall H | Version: " + bot.getMetaData().getVersion());

        String title = bot.getBotType().getManifestName();
        if (!bot.isPrem())
        {
            title += " Lite";
            breakHandler_BT.setDisable(true);
        }
        else if (bot.isBeta())
        {
            title += " Beta";
        }

        title_LL.setText(title);
        addStatusLabelListener();

        updateBreaks();

        showTime(max_play_LL, max_play_SL.getValue());
        showTime(max_break_LL, max_break_SL.getValue());
        showTime(min_play_LL, min_play_SL.getValue());
        showTime(min_break_LL, min_break_SL.getValue());
        addBreakControlChangeListeners();

        start_BT.setOnAction(getStartBotAction());
        controls_BT.setOnAction(setPreferredPane(control_VB));
        skillHandler_BT.setOnAction(setPreferredPane(skillTracker_VB));
        breakHandler_BT.setOnAction(setPreferredPane(break_GP));
        itemHandler_BT.setOnAction(setPreferredPane(item_VB));
        logs_BT.setOnAction(setPreferredPane(logs_VB));

        botSpecificInitialize();
        startLoopingThread();

        panes.add(skillTracker_VB);
        panes.add(break_GP);
        panes.add(item_VB);
        panes.add(logs_VB);
        panes.add(control_VB);
        showPane(control_VB);

        if (bot.isPrem() && Game.isLoggedIn(true))
            autoFill();

        if (chartList == null) chartList = FXCollections.observableArrayList(chartMap.values());
        skillsChart_PC.setData(chartList);

        setStatus("Waiting for the user to start."); // sometimes ui loads after startup
    }

    protected abstract void botSpecificInitialize();
    protected abstract void uiSpecificStart();
    protected abstract String getResourceStream(); // override or declare in manifest
    protected abstract void updateBotSpecificValues();
    protected abstract void updateBotSpecificElements();
    protected abstract void autoFill();

    private String findResourceStream() {
        String defaultResourceStream = "com/kendallhester/personal_api/api/framework/bots/universal/ui/base.fxml";
        return getResourceStream() != null ? getResourceStream() : defaultResourceStream; }
    public APILoopingStructure getBot() { return bot; }
    protected final boolean setStatus(String s) { return getBot().setStatus(s);}
    protected final boolean setStatus(String s, int level) { return getBot().setStatus(s, level); }
    protected final boolean showPane(Pane x) {
        for (Pane i : panes) {
            i.setDisable(true);
            i.setVisible(false);
        }
        x.setVisible(true);
        x.setDisable(false);
        return x.isVisible();
    }

    private EventHandler<ActionEvent> setPreferredPane(Pane x) {
        return event -> showPane(x);
    }

    private void startLoopingThread() {
        if (bot == null) return;
        new LoopingThread(() -> bot.getPlatform().invokeLater(() -> {
            status = bot.getStatus();
            runtimeString = bot.getRuntimeString();
            online = bot.getOnlineRuntime();
            logs = bot.getLogQueue();
            profit = bot.getItemListener().getPrettyProfitString();
            updateBotSpecificValues();
            getSkillTrackerValues();
            getItemTrackerValues();

            Platform.runLater(() -> {

                //stats tab
                runtime_LL.setText(runtimeString);
                status_LL.setText(status);
                statusList_LV.getItems().setAll(logs);
                profit_LL.setText(profit);
                updateSkillTrackerElements();
                updateItemTrackerElements();
                updateBotSpecificElements();
            });
        }), 500).start();
    }

    protected EventHandler<ActionEvent> getStartBotAction() {
        return event -> {
            uiSpecificStart();
            bot.setWait(false);
//            showPane(skillTracker_VB); // need to adjust this so per bot can decide which is the preferred pane
            start_BT.setText("Change Settings");
            GameEvents.Universal.LOGIN_HANDLER.enable();
            GameEvents.Universal.LOBBY_HANDLER.enable();
        };
    }

    public void addStatusLabelListener() {
        Font f = status_LL.getFont();
        status_LL.textProperty().addListener((observable, oldValue, newValue) -> {
            Text tmpText = new Text(newValue);
            tmpText.setFont(f);
            double textWidth = tmpText.getLayoutBounds().getWidth();

            if (textWidth <= 800) {
                status_LL.setFont(f);
            } else {
                double newFontSize = 36 * 800 / textWidth;
                status_LL.setFont(Font.font(f.getFamily(), newFontSize));
            }
        });
    }

    /*
     * Skills section
     */

    private void getSkillTrackerValues() {
        skillListener = getBot().getSkillListener();
    }

    private void updateSkillTrackerElements() {
        if (targetSkill != null && skillListener.getSkillTrackerFor(targetSkill) != null)
        {
            focusedSkill_LL.setText(StringUtil.formatCapitalize(targetSkill.toString()) + ":");
            SkillTracker tracker = skillListener.getSkillTrackerFor(targetSkill);
            exp_LL.setText(UniversalFormat.format(tracker.getExpGained()));
            expPerHour_LL.setText(String.format("%s/hr", UniversalFormat.format(tracker.getExpPerHour())));
            levels_LL.setText(String.format("+%s", UniversalFormat.format(tracker.getLevelsGained())));
            currentExp_LL.setText(UniversalFormat.format(tracker.getExperienceTilLevel()));
            timeTilLevel_LL.setText(tracker.getTimeTil());
        } else {
            focusedSkill_LL.setText("Total:");
            exp_LL.setText(UniversalFormat.format(skillListener.getTotalExperienceGain()));
            expPerHour_LL.setText(String.format("%s/hr", UniversalFormat.format(skillListener.getTotalXpPerHour())));
            levels_LL.setText(String.format("+%s", UniversalFormat.format(skillListener.getTotalLevelsGained())));
            currentExp_LL.setText("N/A");
            timeTilLevel_LL.setText(skillListener.getClosestLevelEstimate());
        }

        resetSkillChartValues(false);
        if (chartList.size() > 0 || chartMap.size() > 0) {
            updateSkillChartValues();
            updatePieChart();
        }
    }

    private void updatePieChart() {
        for(PieChart.Data data : skillsChart_PC.getData()) {
            if (data.getNode().getOnMouseClicked() == null)
                data.getNode().setOnMouseClicked(onMouseClickedChart(data));

            if (setHoverProperty) { // need to set the hover changeListener to something useful like showign they can click
                // maybe hover skill, so you can hover and see the labels change, but they change back if you stop hovering
                // and if you click, they stay static (aka updating with the mouseClickEvent above).
//                data.getNode().hoverProperty().addListener((observable, oldValue, newValue) -> {
//                    data.getNode().getScene().getRoot().setMouseTransparent(!newValue);
////                    data.getNode().setMouseTransparent(newValue);
//                });
            }
        }
        setHoverProperty = false;
    }

    private EventHandler<MouseEvent> onMouseClickedChart(PieChart.Data data) {
        return event -> {
            if (data != null) {
                SkillTracker i = skillListener.tryToParseTracker(data.getName());
                if (i != null) {
                    if (targetSkill == i.getSkill())
                        targetSkill = null;
                    else targetSkill = i.getSkill();
                }
            }
        };
    }

    private void getItemTrackerValues() {
        observableItemList.setAll(getBot().getItemListener().getTrackers());
    }

    private void updateItemTrackerElements() {
        items_TV.setItems(observableItemList);
    }

    private void updateSkillChartValues() {
        needToUpdateChart = skillListener.getSize() != chartList.size();
        boolean notInTracker = false;
        boolean hasLeveledAtAll = false;
        boolean notInMap = false;

        //update chart first
        for (PieChart.Data d : chartList) {
            SkillTracker tracker = skillListener.tryToParseTracker(d.getName());
                if (tracker == null) notInTracker = true;
                else {
                    Skill s = tracker.getSkill();
                    boolean hasLeveled = !skillLevelMap.containsKey(s) || skillLevelMap.get(s) < skillListener.getBaseLevelOf(s);
                    if (hasLeveled) {
                        hasLeveledAtAll = true;
                        skillLevelMap.put(s, skillListener.getBaseLevelOf(s));
                        d.setName(tracker.toString());
                    }
                    d.setPieValue((double) tracker.getExpGained() / (double) skillListener.getTotalExperienceGain());
                }
//            }
        }

        if (notInTracker || needToUpdateChart || hasLeveledAtAll) {
            resetSkillChartValues(true);
        }
    }

    private void resetSkillChartValues(boolean force) {
        needToUpdateChart = skillListener.getSize() != chartList.size();
        for (SkillTracker i : skillListener.getTrackers()) {
            boolean hadKey = false;
            double oldRatio = 0;
            if (chartMap.containsKey(i.getSkill())) {
                hadKey = true;
                oldRatio = chartMap.get(i.getSkill()).getPieValue();
            }
            Skill x = i.getSkill();
            double ratio = (double) i.getExpGained() / (double) skillListener.getTotalExperienceGain();
            boolean hasLeveled = !skillLevelMap.containsKey(x) || skillLevelMap.get(x) > skillListener.getCurrentLevelOf(x);
            needToUpdateChart = needToUpdateChart || hasLeveled || (hadKey && oldRatio != ratio);
            chartMap.put(x, new PieChart.Data(i.toString(), ratio));
        }

        if (force || skillListener.getSize() != chartList.size()) {
            chartList.setAll(chartMap.values());
            setHoverProperty = true;
        }
    }

    /**
     * ItemTracker
     */
    private EventHandler<ActionEvent> resetItem() {
        return event -> {
            ItemTracker selection = items_TV.getSelectionModel().getSelectedItem();
            if (selection != null) {
                getBot().getItemListener().deleteTracker(selection);
            }
        };
    }

    private EventHandler<ActionEvent> resetAllItems() {
        return event -> getBot().getItemListener().empty();
    }

    private EventHandler<ActionEvent> toggleDev() {
        return event -> {
            setStatus("Toggling dev mode.", 1);
            getBot().toggleDev();
        };
    }

    /**
     * New breaks
     */
    private EventHandler<ActionEvent> clearFields() {
        return event -> {
            start_TF.clear();
            end_TF.clear();
        };
    }

    private EventHandler<ActionEvent> saveBreaks() {
        return event -> bot.getPlatform().invokeLater(bot::saveBreaks);
    }

    private EventHandler<ActionEvent> loadBreaks() {
        return event -> {
            try {
                bot.getPlatform().invokeAndWait(bot::loadBreaks);
            } catch (Exception e) {
                e.printStackTrace();
            }
            updateBreaks();
            if (bot.getBreaker().getSize() > 0) {
                deleteBreak_BT.setDisable(false);
                bot.setConfig("breaks",true);
            }
        };
    }

    private EventHandler<ActionEvent> createRandBreaks() {
        return event -> {
            BreakHandler handler = bot.getBreaker();
            long old = handler.getSize() == 0 ? 0 : handler.getBreakAt(handler.getSize()-1).getEnd();
            for (int i = 0; i < breakAmount_SP.getValue(); i++) {
                Break e = new Break(handler.getCurrentIndex()+1,
                        old + MathTools.randLong((min_play_SL.getValue() * 1800000),
                                (max_play_SL.getValue() * 1800000)),
                        MathTools.randLong(min_break_SL.getValue() * 1800000, (int) max_break_SL.getValue() * 1800000));
                handler.addBreak(e);
                handler.resortList();
                old = e.getEnd();
            }

            if (bot.getBreaker().getSize() > 0) {
                deleteBreak_BT.setDisable(false);
                bot.setConfig("breaks",true);
            }

            updateBreaks();
        };
    }

    private EventHandler<ActionEvent> createSpecificBreak(boolean append) {
        return event -> {
            String start = start_TF.getText();
            String end = end_TF.getText();
            if (start != null && end != null) {
                Break e = bot.getBreaker().createBreak(start, end, append);
                if (e != null) {
                    updateBreaks();
                    bot.setConfig("breaks",true);
                    deleteBreak_BT.setDisable(false);
                }
            }
        };
    }

    private void showTime(Label field, Number val) {
        field.setText("" + Time.format(val.longValue() * 1800000));
    }

    private void adjustMin(Slider slider, double val) {
        if (slider.getValue() < val)
            slider.setValue(val);
    }

    private void adjustMax(Slider slider, double val) {
        if (slider.getValue() > val)
            slider.setValue(val);
    }

    private void updateBreaks() {
        getBot().getBreaker().resortList();
        observableBreakList.setAll(getBot().getBreaker().getBreakList());
    }

    @FXML
    private EventHandler<ActionEvent> removeBreak() {
        return Event -> {
            Break selection = breaks_TV.getSelectionModel().getSelectedItem();
            if (selection != null) {
                int i = bot.getBreaker().getIndex(selection);
                if (bot.getBreaker().deleteBreak(selection)) {
                    bot.setStatus(String.format("[BreakHandler] - Break %d has been deleted.", (i+1)));
                }
            }

            if (bot.getBreaker().getSize() < 1) {
                bot.setConfig("breaks",false);
                deleteBreak_BT.setDisable(true);
            }
            
            updateBreaks();
        };
    }

    private void addBreakControlListeners(PairList<Slider, Pair<Label, Slider>> pList) {
        for (Pair<Slider, Pair<Label, Slider>> p : pList) {
            p.getLeft().valueProperty().addListener((ov, old_val, new_val) -> {
                showTime(p.getRight().getLeft(), new_val);
                adjustMin(p.getRight().getRight(), new_val.doubleValue());
            });
        }
        sanitizeTimeInput(start_TF, end_TF);
    }

    private void addBreakControlChangeListeners() {
        min_break_SL.valueProperty().addListener((ov, old_val, new_val) -> {
            showTime(min_break_LL, new_val);
            adjustMin(max_break_SL, new_val.doubleValue());
        });
        min_play_SL.valueProperty().addListener((ov, old_val, new_val) -> {
            showTime(min_play_LL, new_val);
            adjustMin(max_play_SL, new_val.doubleValue());
        });
        max_break_SL.valueProperty().addListener((ov, old_val, new_val) -> {
            showTime(max_break_LL, new_val);
            adjustMax(min_break_SL, new_val.doubleValue());
        });

        max_play_SL.valueProperty().addListener((ov, old_val, new_val) -> {
            showTime(max_play_LL, new_val);
            adjustMax(min_play_SL, new_val.doubleValue());
        });

        breakType.selectedToggleProperty().addListener((ov, old_val, new_val) -> adjustVisibleBreakPane(new_val));

        sanitizeTimeInput(start_TF, end_TF);
    }

    private void adjustVisibleBreakPane(Toggle i) {
        RadioButton toggle = (RadioButton) i;
        if (toggle.equals(randomBreak_RB)) {
            customBreak_HB.setVisible(false);
            customBreak_HB.setDisable(true);
            randomBreaks_HB.setVisible(true);
            randomBreaks_HB.setDisable(false);
        }
        else if (toggle.equals(customBreak_RB)) {
            customBreak_HB.setVisible(true);
            customBreak_HB.setDisable(false);
            randomBreaks_HB.setVisible(false);
            randomBreaks_HB.setDisable(true);
        }
    }

    protected boolean isVersionUpToDate(String vers, String needed) { // anywhere from 3-4 length
        String[] versArray = vers.split("\\.");
        String[] neededArray = needed.split("\\.");

        List<Integer> received = new ArrayList<>();
        for (String i : versArray) { received.add(StringUtil.parseInt(i)); }

        List<Integer> minimum = new ArrayList<>();
        for (String i : neededArray) { minimum.add(StringUtil.parseInt(i)); }

        if (received.get(0) > minimum.get(0)) return true;
        else if (received.get(0).intValue() == minimum.get(0).intValue() && received.get(1) > minimum.get(1)) return true;

        boolean majorAndMinor = received.get(0) >= minimum.get(0) && received.get(1) >= minimum.get(1);
        if (received.size() >= minimum.size()) {
            return majorAndMinor && received.get(2) >= minimum.get(2);
        } else {
            return majorAndMinor && received.get(2) > minimum.get(2);
        }
    }

    private static void sanitizeTimeInput(TextField... tfs) {
        for (TextField tf : tfs) {
            tf.textProperty().addListener(((observable, oldValue, newValue) -> {
                if (newValue.length() > 11) {
                    tf.setText(oldValue);
                } else {
                    if (newValue.length() != 11) {
                         if (!newValue.matches("\\d") && !newValue.matches("\\d\\d:")) {
                            tf.setText(newValue.replaceAll("[^\\d:]", ""));
                        }
                    }
                }
            }));
        }
    }
}
