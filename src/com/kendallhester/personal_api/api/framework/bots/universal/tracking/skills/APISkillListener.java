package com.kendallhester.personal_api.api.framework.bots.universal.tracking.skills;

import com.kendallhester.personal_api.api.framework.bots.looping.APILoopingStructure;
import com.kendallhester.personal_api.api.framework.bots.universal.tracking.APIListener;
import com.kendallhester.personal_api.api.framework.utils.formatting.UniversalFormat;
import com.kendallhester.personal_api.api.framework.utils.misc.MathTools;
import com.kendallhester.personal_api.api.game.skills.SkillUniversal;
import com.gameclient.game.api.hybrid.local.Skill;
import com.gameclient.game.api.hybrid.util.Time;
import com.gameclient.game.api.script.framework.listeners.SkillListener;
import com.gameclient.game.api.script.framework.listeners.events.SkillEvent;

import java.util.*;

public class APISkillListener extends APIListener implements SkillListener {

    private final HashMap<Skill, SkillTracker> trackers = new HashMap<>();

    public APISkillListener(APILoopingStructure b) {
        this.bot = b;
        bot.getEventDispatcher().addListener(this);
    }

    @Override
    public void onLevelUp(SkillEvent e) {
        if (e != null && e.getChange() > 0) {
            if (!checkIsRunning()) return;
            SkillTracker targ = getSkillTrackerFor(e.getSkill());
            if (targ != null) {
                targ.incLevel(e.getChange());
                targ.updateValues();
                bot.botSpecificSkillEventHandling(e.getSkill(), e.getChange(), false);
            }
        }
    }

    @Override
    public void onExperienceGained(SkillEvent e) {
        if (e != null && e.getChange() > 0) {
            if (!checkIsRunning()) return;
            SkillTracker targ = getSkillTrackerFor(e.getSkill());
            if (targ != null) {
                targ.incExp(e.getChange());
                targ.updateValues();
                bot.botSpecificSkillEventHandling(e.getSkill(), e.getChange(), true);
            }
            else {
                if (getSkillTrackerFor(e.getSkill()) == null) { // check again, been seeing duplicates
                    bot.setStatus("Tracking: " + e.getSkill(), 1);
                    trackers.putIfAbsent(e.getSkill(), new SkillTracker(e.getSkill(), e.getChange(), true));
                    bot.botSpecificSkillEventHandling(e.getSkill(), e.getChange(), true);
                }
            }
        }
    }

    @Override
    public int getSize() {
        return trackers.size();
    }
    public final Set<SkillTracker> getTrackers() {
        return new HashSet<>(trackers.values());
    }

    public final void fillFakeTrackers(Skill... sk) { // testing
        for (Skill i : sk) {
            trackers.put(i, new SkillTracker(i, MathTools.randInt(1, 500000), true));
        }
    }

    @Override
    public void pauseTrackers() {
        for (SkillTracker x : getTrackers()) {
            x.pause();
        }
    }

    @Override
    public void resumeTrackers() {
        for (SkillTracker x : getTrackers()) {
            x.resume();
        }
    }

    public SkillTracker getSkillTrackerFor(Skill sk) {
        return trackers.get(sk);
    }

    public int getBaseLevelOf(Skill sk) {
        SkillTracker x = getSkillTrackerFor(sk);
        return x != null ? x.getBaseLevel() : -1;
    }

    public int getCurrentLevelOf(Skill sk) {
        SkillTracker x = getSkillTrackerFor(sk);
        return x != null ? x.getCurrentLevel() : -1;
    }

    public int getExperienceGainedFor(Skill sk) {
        SkillTracker x = getSkillTrackerFor(sk);
        if (x != null) return x.getExpGained();
        return -1;
    }

    public long getTotalExperienceGain() {
        return getTrackers().stream().mapToLong(SkillTracker::getExpGained).sum();
    }

    public int getTotalLevelsGained() {
        return getTrackers().stream().mapToInt(SkillTracker::getLevelsGained).sum();
    }

    public long getTotalXpPerHour() { return getTrackers().stream().mapToLong(SkillTracker::getExpPerHour).sum(); }

    public String getClosestLevelEstimate() {
        Set<SkillTracker> sTrackers = getTrackers();
        if (sTrackers.size() > 0) {
            SkillTracker i = sTrackers.stream().min(Comparator.comparingInt(SkillTracker::getTimeTilAsValue)).orElse(null);
            return i.getTimeTil();
        }
        return "00:00:00";
    }

    public void printTotalExperienceGain() {
        long xpGain = getTotalExperienceGain();
        String xpEarned = xpGain > 1000000 ? UniversalFormat.format(xpGain, 1) : UniversalFormat.format(xpGain);
        bot.setStatus(String.format("Total gained: %s (%s/hr)",
                xpEarned, UniversalFormat.formatPerHour(getTime(), getTotalExperienceGain())));
    }

    public void printStats() {
        for (SkillTracker x : getTrackers()) {
            bot.setStatus(x.printProgress());
        }
    }

    public SkillTracker tryToParseTracker(String x) {
        if (x == null) return null;
        SkillTracker tr = getTrackers().stream().filter(i -> i.toString().equals(x)).findFirst().orElse(null);
        if (tr == null) {
            String skill = x.split(":")[0];
            Skill sk = SkillUniversal.determineSkillFromString(skill);
            tr = getSkillTrackerFor(sk);
        }
        return tr;
    }

    @Override
    public void empty() {
        trackers.clear();
        bot.setStatus("Clearing sk-tk.", 1);
    }
}
