package com.kendallhester.personal_api.api.framework.bots.universal.tracking;

import com.kendallhester.personal_api.api.framework.bots.looping.APILoopingStructure;

public abstract class APIListener {

    protected APILoopingStructure bot;

    public abstract int getSize();
    public abstract void pauseTrackers();
    public abstract void resumeTrackers();
    public abstract void empty();

    public final long getTime() {
        return bot == null ? -1 : bot.getOnlineRuntime();
    }

    public final boolean checkIsRunning() {
        return bot != null && bot.isRunning() && !bot.isWaiting();
    }
}
