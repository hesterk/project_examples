package com.kendallhester.personal_api.api.framework.bots.universal.resources;

import com.gameclient.game.api.hybrid.Environment;
import com.gameclient.game.api.hybrid.player_sense.PlayerVariation;
import com.gameclient.game.api.hybrid.util.calculations.Random;

import java.util.function.Supplier;

public class CustomPlayerVariation {

    public static void initializeKeys() {
        for (Key key : Key.values()) {
            if (key != null) {
                try {
                    if (PlayerVariation.get(key.name) == null) {
                        PlayerVariation.put(key.name, key.supplier.get());
                    }
                } catch (Exception e) {
                    Environment.getLogger().debug("Exception when beginning variation..");
                }
            }
        }
    }

    public enum Key {
        PRIMARY_SORTING_WEIGHT("primary_sorting_weight", () -> Random.nextDouble(.49, .82)),
        PRIMARY_SORTING_TYPE("sorting_preference", () -> Random.nextInt(1,6)),
        CHANCE_OF_AFK("typical_afk_chance", () -> Random.nextDouble(.02,.09)),
        LENGTH_OF_AFK("typical_afk_length", () -> Random.nextLong(7000, 22000)),
        TIME_BEFORE_AFK("typical_afk_precursor", () -> Random.nextLong(700000, 11000000)),
        TENDENCY_TO_CHECK_LEVEL("check_level_tendency", () -> Random.nextDouble(0.08, 0.11)),
        TENDENCY_TO_RANDOMLY_TURN("random_turn_tendency", () -> Random.nextDouble(.01, .19)),
        TIME_BEFORE_TURNING("typical_turn_precursor", () -> Random.nextLong(140000, 321920)),
        TENDENCY_TO_BE_PRODUCTIVE("productivity_rate", () -> Random.nextDouble(.21, .72));

        private final String name;
        private final Supplier supplier;

        Key(String name, Supplier supplier) {
            this.name = name;
            this.supplier = supplier;
        }

        public final String getKey() {
            return name;
        }
        public final Integer getAsInteger() {
            return PlayerVariation.getAsInteger(name);
        }
        public final Double getAsDouble() {
            return PlayerVariation.getAsDouble(name);
        }
        public final Long getAsLong() {
            return PlayerVariation.getAsLong(name);
        }
        public final Boolean getAsBoolean() {
            return PlayerVariation.getAsBoolean(name);
        }

    }
}
