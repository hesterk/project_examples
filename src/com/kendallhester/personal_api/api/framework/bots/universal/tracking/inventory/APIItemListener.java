package com.kendallhester.personal_api.api.framework.bots.universal.tracking.inventory;

import com.kendallhester.personal_api.api.framework.bots.looping.APILoopingStructure;
import com.kendallhester.personal_api.api.framework.bots.universal.tracking.APIListener;
import com.kendallhester.personal_api.api.framework.utils.formatting.StringUtil;
import com.kendallhester.personal_api.api.framework.utils.formatting.UniversalFormat;
import com.kendallhester.personal_api.api.game.interfaces.Exchange;
import com.gameclient.game.api.hybrid.entities.definitions.ItemDefinition;
import com.gameclient.game.api.hybrid.local.hud.interfaces.Bank;
import com.gameclient.game.api.script.framework.listeners.InventoryListener;
import com.gameclient.game.api.script.framework.listeners.events.ItemEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class APIItemListener extends APIListener implements InventoryListener {

    private final HashMap<Integer, ItemTracker> trackerMap = new HashMap<>();

    public APIItemListener(APILoopingStructure b) {
        this.bot = b;
        bot.getEventDispatcher().addListener(this);
    }

    @Override
    public void onItemRemoved(ItemEvent e) {
        if (e != null && e.getQuantityChange() != 0) {
            if (!checkIsRunning()) return;
            if (!Bank.isOpen()) {
                ItemDefinition def = e.getItem().getDefinition();
                if (def != null) {
                    ItemTracker i = getTrackerFor(def.getId());
                    if (i != null) { i.addItemsRemoved(e.getQuantityChange()); }
                    else {
                        bot.setStatus(String.format("Now following - %s (%d)", def.getName(), def.getId()), 1);
                        trackerMap.putIfAbsent(def.getId(), new ItemTracker(def, searchPrice(def), e.getQuantityChange(), false));
                    }
                    bot.botSpecificItemEventHandling(def, e.getQuantityChange(), false);
                }
            }
        }
    }

    @Override
    public void onItemAdded(ItemEvent e) {
        if (e != null && e.getQuantityChange() != 0) {
            if (!checkIsRunning()) return;
            if (!Bank.isOpen()) {
                ItemDefinition def = e.getItem().getDefinition();
                if (def != null) {
                    ItemTracker i = getTrackerFor(def.getId());
                    if (i != null) { i.addItemsReceived(e.getQuantityChange()); }
                    else {
                        bot.setStatus(String.format("Now following - %s (%d)", def.getName(), def.getId()), 1);
                        trackerMap.putIfAbsent(def.getId(), new ItemTracker(def, searchPrice(def), e.getQuantityChange(), true));
                    }
                    bot.botSpecificItemEventHandling(def, e.getQuantityChange(), true);
                }
            }
        }
    }

    public ItemTracker getTrackerFor(int id) { return trackerMap.get(id); }
    public List<ItemTracker> getTrackers() { return new ArrayList<>(trackerMap.values()); }

    public int getItemAmount(int id) {
        if (id != -1) {
            ItemTracker target = getTrackerFor(id);
            if (target != null) return target.getAmount();
        }
        return 0;
    }

    public int getItemAmount(String name, boolean noted) {
        if (StringUtil.exists(name)) {
            ItemTracker target = getTrackers().stream()
                    .filter(i -> i.getName().equalsIgnoreCase(name) && i.isNoted() == noted)
                    .findFirst().orElse(null);
            if (target != null) return target.getAmount();
        }
        return 0;
    }


    public long getTotalGoldChanges() {
        return getTrackers().stream().mapToLong(ItemTracker::getGoldChange).sum();
    }

    public String getPrettyProfitString() {
        String direction = getTotalGoldChanges() < 0 ? "" : "+";
        return String.format("%s (%s%s gp/hour)",
                UniversalFormat.formatAsGp(getTotalGoldChanges()),
                direction,
                UniversalFormat.formatPerHour(getTime(), getTotalGoldChanges()));
    }

    public void printTotalGoldChanges() {
        String direction = getTotalGoldChanges() < 0 ? "lost" : "gained";
        bot.setStatus(String.format("Total %s: %s gold (%s/hr)",
                direction,
                UniversalFormat.formatAsGp(getTotalGoldChanges()),
                UniversalFormat.formatAsGpPerHour(getTime(), getTotalGoldChanges(), 1)));
    }

    private int searchPrice(ItemDefinition def) {
        if (def != null) {
            if (def.getName().equals("Coins"))
                return 1;
            else if (def.getName().equals("Hunter kit"))
                return 860;
            else if (def.isTradeable())
                return Exchange.checkPrice(def.getId());
        }
        return 0;
    }

    @Override
    public int getSize() {
        return (int) getTrackers().stream().filter(ItemTracker::isItemTradeable).count();
    }

    @Override
    public void pauseTrackers() {
        getTrackers().forEach(ItemTracker::pause);
    }

    @Override
    public void resumeTrackers() {
        getTrackers().forEach(ItemTracker::resume);
    }

    public void deleteTracker(ItemTracker i) {
        int itemID = i.getItemId();
        if (trackerMap.remove(itemID, i)) {
            String extraInfo = "";
            if (i.isNoted()) extraInfo = "(Noted)";
            bot.setStatus(String.format("Deleted time-tracking for: %s (%d) %s", i.getName(), i.getItemId(), extraInfo), 1);
        }
    }

    @Override
    public void empty() {
        trackerMap.clear();
        bot.setStatus("Cleared items.", 1);
    }
}
