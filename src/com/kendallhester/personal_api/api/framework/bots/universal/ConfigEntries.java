package com.kendallhester.personal_api.api.framework.bots.universal;

public class ConfigEntries {

    // TODO: Finish this class and switch from using strings.
    public interface ConfigItem {
        String getCode();
        String getDescription();
    }

    /**
     * Universal configs. Apply to all scripts/bots.
     */
    public enum UniversalConfig implements ConfigItem {

        /**
         * Tracks if the current bot being used is in a beta state.
         */
        BETA("beta", "Tracks if the current bot being used is in beta"),

        /**
         * Tracks if the current bot supports breaks AND has them enabled.
         */
        BREAKS("breaks", "Tracks if the bot currently supports breaks and has them enabled."),

        /**
         * Tracks if the current user is treated as a developer (i.e. will bot run in developer mode.)
         */
        DEVELOPER("dev", "Tracks if the current user is a developer or not."),

        /**
         * Tracks if the current bot being used is a premium (paid) bot or should be treated as such.
         */
        PREMIUM("premium", "Tracks if the current bot being used is a premium (paid) bot");

        private final String code;
        private final String description;

        UniversalConfig(String cd, String desc) {
            this.code = cd;
            this.description = desc;
        }

        public String getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }

    }


    /**
     * Common configs. Apply to many bots.
     */
    public enum CommonConfig implements ConfigItem {
        ;

        @Override
        public String getCode() {
            return null;
        }

        @Override
        public String getDescription() {
            return null;
        }
    }

}
