package com.kendallhester.personal_api.api.framework.bots.universal.tracking.inventory;

import com.kendallhester.personal_api.api.framework.utils.formatting.UniversalFormat;
import com.gameclient.game.api.hybrid.entities.definitions.ItemDefinition;
import com.gameclient.game.api.hybrid.util.StopWatch;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemTracker {

    private final ItemDefinition definition;
    private int price = 0;
    private int itemId = 0;
    private final String name;
    private final StopWatch timeTracker;
    private final boolean isTradeable;
    private boolean isNoted = false;

    private StringProperty idAsString, priceAsString, isNotedAsString, amountAsString, goldChangeAsString, timeAsString;
    private int amount, goldChange;

    public ItemTracker(ItemDefinition itemDefinition, int gePrice, int quantityChange, boolean received) {
        definition = itemDefinition;
        name = itemDefinition.getName();
        isTradeable = itemDefinition.isTradeable();

        setAmount(quantityChange, received);
        setPrice(gePrice);
        setGoldChange(quantityChange * gePrice, received);
        setId(itemDefinition.getId());
        setNoted(itemDefinition.isNoted());

        timeTracker = new StopWatch();
        timeTracker.start();


    }

    public void setPrice(int gePrice) {
        if (priceAsString == null) priceAsString = new SimpleStringProperty();
        price = gePrice;
        priceAsString.set(String.format("%s gold", UniversalFormat.format(price)));
    }

    public void setId(int id) {
        if (idAsString == null) idAsString = new SimpleStringProperty();
        this.itemId = id;
        idAsString.set(String.format("ID: %d", id));
    }

    public void setNoted(boolean shouldBeNoted) {
        if (isNotedAsString == null) isNotedAsString = new SimpleStringProperty();
        this.isNoted = shouldBeNoted;
        String noted = isNoted ? "Noted" : "Normal";
        isNotedAsString.set(noted);
    }

    public void setAmount(int change, boolean up) {
        if (amountAsString == null) amountAsString = new SimpleStringProperty();
        if (up) amount += change; else amount-=change;
        amountAsString.set(String.format("%s", UniversalFormat.format(amount)));
    }

    public void setGoldChange(int change, boolean up) {
        if (goldChangeAsString == null) goldChangeAsString = new SimpleStringProperty();
        if (up) goldChange += change; else goldChange -= change;
        goldChangeAsString.set(UniversalFormat.formatAsGp(goldChange));
    }

    public void pause() {
        if (timeTracker.isRunning())
            timeTracker.stop();
    }

    public void resume() {
        if (!timeTracker.isRunning())
            timeTracker.start();
    }

    public String getIsNotedAsString() { return isNotedAsString.get(); }
    public String getIdAsString() { return idAsString.get(); }
    public String getPriceAsString() { return priceAsString.get(); }
    public String getAmountAsString() { return amountAsString.get(); }
    public String getGoldChangeAsString() { return goldChangeAsString.get(); }
    public String getTimeAsString() { return timeTracker.getRuntimeAsString(); }
    public StopWatch getTimeTracker() { return timeTracker; }
    public boolean isItemTradeable() { return isTradeable; }
    public int getItemId() { return itemId; }
    public int getPrice() { return price; }
    public String getName() { return name; }
    public boolean isNoted() { return isNoted; }
    public int getAmount() { return amount; }
    public ItemDefinition getDefinition() { return definition; }
    public int getGoldChange() { return goldChange; }
    public void addItemsReceived(int val) { setAmount(val, true); setGoldChange(val*price, true);}
    public void addItemsRemoved(int val) { setAmount(val, false); setGoldChange(val*price, false);}
}
