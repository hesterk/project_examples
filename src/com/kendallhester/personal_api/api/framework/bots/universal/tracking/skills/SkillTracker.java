package com.kendallhester.personal_api.api.framework.bots.universal.tracking.skills;

import com.kendallhester.personal_api.api.framework.utils.formatting.UniversalFormat;
import com.gameclient.game.api.hybrid.local.Skill;
import com.gameclient.game.api.hybrid.local.Skills;
import com.gameclient.game.api.hybrid.util.StopWatch;
import com.gameclient.game.api.hybrid.util.calculations.CommonMath;

import java.util.concurrent.TimeUnit;

public class SkillTracker {

    private final Skill skill;
    private final StopWatch timeTracker;
    private int expGained, levelsGained, baseLevel;

    private int currentLevel, currentExp, expTilLevel, expTilLevelAsPercent;

    public SkillTracker(Skill skill, int change, boolean expChange) {
        this.skill = skill;
        this.timeTracker = new StopWatch();
        updateValues();
        timeTracker.start();

        if (expChange) expGained += change;
        else levelsGained += change;
    }

    public Skill getSkill() {
        return skill;
    }

    public int getExpTilLevelAsPercent() { return expTilLevelAsPercent;}
    public int getCurrentExp() { return currentExp; }
    public int getExpGained() {
        return expGained;
    }
    public int getLevelsGained() {
        return levelsGained;
    }
    public int getBaseLevel() { return baseLevel; }
    public int getCurrentLevel() {
        return currentLevel;
    }
    public int getExperienceTilLevel() {
        return expTilLevel;
    }

    public long getExpPerHour() {
        return (long) CommonMath.rate(TimeUnit.HOURS, timeTracker.getRuntime(), expGained);
    }

    public long getLevelsPerHour() {
        return (long) CommonMath.rate(TimeUnit.HOURS, timeTracker.getRuntime(), levelsGained);
    }

    public String getTimeTil() { return UniversalFormat.getTimeTil(getExpPerHour(), getExperienceTilLevel());
    }

    public int getTimeTilAsValue() { return UniversalFormat.getTimeTilAsValue(getExpPerHour(), getExperienceTilLevel()); }

    ///

    public void updateValues() {
        currentLevel = getSkill().getCurrentLevel();
        baseLevel = getSkill().getBaseLevel();
        currentExp = getSkill().getExperience();
        expTilLevel = getSkill().getExperienceToNextLevel();
        expTilLevelAsPercent = getSkill().getExperienceToNextLevelAsPercent();
    }
    public void incExp(long amt) { expGained+=amt;}
    public void incLevel(long amt) {
        levelsGained+=amt;
    }
//    public void setLastEvent(long runtime) { lastXpEvent = runtime; } // tracked outside of the individual tracking time

    public void pause() {
        if (timeTracker.isRunning())
            timeTracker.stop();
    }

    public void resume() {
        if (!timeTracker.isRunning())
            timeTracker.start();
    }

    public String printProgress() {
        return String.format("%s: %s gained (%s/hr) and %d intervals increased (%d/hr)",
                getSkill().toString(),
                UniversalFormat.format(getExpGained()),
                UniversalFormat.format(getExpPerHour(), 1),
                getLevelsGained(),
                getLevelsPerHour());
    }

    @Override
    public String toString() { return String.format("%s: %d (+%d)", getSkill().name(), getBaseLevel(), getLevelsGained()); }

}
