package com.kendallhester.personal_api.api.framework.bots.universal.resources;


import com.kendallhester.personal_api.api.framework.bots.looping.BotType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Class for people who have been blacklisted from bots (staff approved and suggested), and added to permanent
 * premium or beta testing groups
 */
public class Users {

    private static final HashMap<BotType, List<String>> freeMap = new HashMap<>();
    private static final HashMap<BotType, List<String> >betaMap = new HashMap<>();

    static {
        // free
        freeMap.put(BotType.MINING, Arrays.asList("User1", "User2", "User3"));
//        freeMap.put(BotType.MINIGAME, Arrays.asList("User1", "Developer"));

        // beta
        betaMap.put(BotType.MINING, Arrays.asList("User3", "Developer"));
    }

    private final static Set<String> bannedNames = Set.of("test", "test2");
    private final static Set<Integer> bannedIds = Set.of(123, 456);

    public static boolean checkIfBetaTesting(BotType bot, String user) {
        List<String> users = betaMap.get(bot);
        return users != null && users.contains(user);
    }

    public static boolean checkIfFreePremium(BotType bot, String user) {
        List<String> users = freeMap.get(bot);
        return users != null && users.contains(user);
    }

    public static boolean checkIfBanned(String name, int id) {
        return isBanned(name) || isBanned(id);
    }

    private static boolean isBanned(String forumName) {
        return bannedNames.stream().anyMatch(i -> i.equalsIgnoreCase(forumName));
    }

    private static boolean isBanned(int forumId){
        return bannedIds.stream().anyMatch(i -> i == forumId);
    }
}