package com.kendallhester.personal_api.api.framework.bots.tree.framework;

public abstract class TreeLeaf extends TreeNode {

    @Override
    public boolean validate() {
        return false; // unused
    }

    @Override
    public TreeNode successTask() {
        return null; // unused
    }

    @Override
    public TreeNode failureTask() {
        return null; // unused
    }

    @Override
    public boolean isLeaf() {
        return true; // unused
    }
}
