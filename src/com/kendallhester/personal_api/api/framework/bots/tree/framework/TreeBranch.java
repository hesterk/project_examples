package com.kendallhester.personal_api.api.framework.bots.tree.framework;

public abstract class TreeBranch extends TreeNode {

    @Override
    public void execute() {
        // unused.
    }

    @Override
    public boolean isLeaf() {
        return false;
    }
}
