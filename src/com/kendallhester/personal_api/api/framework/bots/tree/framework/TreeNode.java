package com.kendallhester.personal_api.api.framework.bots.tree.framework;

import com.kendallhester.bots.tree_structures.mining_example.main.Mining;
import com.kendallhester.personal_api.api.framework.bots.looping.APILoopingStructure;
import com.gameclient.game.api.hybrid.Environment;

public abstract class TreeNode {

    public final TreeStructure b = (TreeStructure) Environment.getBot();

    TreeNode() { } // i can do the same thing lol

    public abstract boolean validate();
    public abstract void execute();
    public abstract TreeNode successTask();
    public abstract TreeNode failureTask();
    public abstract boolean isLeaf();

    protected boolean preValidate() { return true;} // override

    public TreeStructure getBot() {
        assert b != null;
        return b;
    }

    @SuppressWarnings("unchecked")
    public <T extends APILoopingStructure> T getBotObject() {
        assert b != null;
        return (T)getBotObject(b.getBotType().getType());
    }

    private <T extends APILoopingStructure> T getBotObject(Class<T> type) {
        assert b != null;
        return type.cast(b);
    }

    public boolean logDev(String x) { return b != null && b.logDev(x); }
    public boolean setStatus(String x) {
        return b != null && b.setStatus(x);
    }
    public boolean setStatus(String x, int level) {
        return b != null && b.setStatus(x, level);
    }

    /**
     * Returns an instance of APILoopingStructure as the Mining bot if the bot that calls this class is
     * an instance of Mining bot.
     * if not, returns null.
     * @return APILoopingStructure as Mining instance
     */
    public final Mining getMiningBot() {
        if (getBot() instanceof Mining)
            return getBotObject();
        return null;
    }
}
