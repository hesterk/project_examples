package com.kendallhester.personal_api.api.framework.bots.tree.framework;

import com.kendallhester.personal_api.api.framework.bots.universal.resources.Users;
import com.kendallhester.personal_api.api.framework.utils.formatting.UniversalFormat;
import com.kendallhester.personal_api.api.framework.utils.overlays.JFXOverlay;
import com.kendallhester.personal_api.api.framework.bots.looping.APILoopingStructure;
import com.gameclient.game.api.hybrid.Environment;
import com.gameclient.game.api.script.Execution;
import javafx.application.Platform;

public abstract class TreeStructure extends APILoopingStructure {

    private TreeNode root;
    protected TreeStructure() {
        super();
    }

    @Override
    public final void onLoop() {
        if (root == null) {
            root = createRootTask();
            if (root == null) {
                setStatus("Failed to create the root node of the bot. Results in null.");
                end();
                throw new IllegalStateException(getStatus());
            }
        }
        TreeNode currentTask = root;
        while (!currentTask.isLeaf() && isRunning()) {

            currentTask.preValidate();
            if (isRunning() && currentTask.validate()) {
                TreeNode successTask = currentTask.successTask();
                if (successTask == null) {
                    throw new UnsupportedOperationException(
                            "Branch(" + currentTask.getClass().getName()
                                    + ") had a null Success Task.");
                } else {
                    currentTask = successTask;
                }
            } else {
                TreeNode failureTask = currentTask.failureTask();
                if (failureTask == null) {
                    throw new UnsupportedOperationException(
                            "Branch(" + currentTask.getClass().getName()
                                    + ") had a null Failure Task.");
                } else {
                    currentTask = failureTask;
                }
            }
        }

        if (isRunning()) currentTask.execute(); // could maybe wrap NPE
    }

    public final TreeNode createRootTask() {
        if (!Execution.delayUntil(() -> !isWaiting(), (int) UniversalFormat.getMinutesAsMillisecs(30))) {
            setStatus("Took too long to set up the GUI options. This bot has been stopped for your safety.", 2);
            end();
            return getBotSpecificRoot();
        }

        if ((!isPrem() && !isBeta()) && Users.checkIfBanned(Environment.getForumName(), Environment.getForumId())) {
            setStatus("You have been blocked from using these free bots :(.", 3);
            Execution.delay(600,800);
            end();
            return getBotSpecificRoot();
        }

        setStatus("Starting bot...");
        startOnlineTimer();

        if (isDev()) Platform.runLater(() -> new JFXOverlay(this)); // only i should see the JFX overlay (not bot-ui)
        return getBotSpecificRoot();
    }

    protected abstract TreeNode getBotSpecificRoot();

}
