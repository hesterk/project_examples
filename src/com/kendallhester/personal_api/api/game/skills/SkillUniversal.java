package com.kendallhester.personal_api.api.game.skills;

import com.kendallhester.personal_api.api.framework.utils.formatting.StringUtil;
import com.gameclient.game.api.hybrid.Environment;
import com.gameclient.game.api.hybrid.local.Skill;

import java.util.Arrays;
import java.util.Objects;

public class SkillUniversal {

    public static Skill determineSkillFromString(String s) {
        if (StringUtil.exists(s)) {
            return Arrays.stream(Skill.values()).filter(i -> i.toString().equalsIgnoreCase(s)).findFirst().orElse(null);
        }
        Objects.requireNonNull(Environment.getBot()).getLogger().info("Passed in a null string.. how.");
        return null;
    }
}
