package com.kendallhester.personal_api.api.game.interfaces;

import com.gameclient.game.api.hybrid.net.GrandExchange;

public class Exchange {

    public static int checkPrice(int i) {
        if (i == 0|| i == -1) {
            return -1;
        }
        GrandExchange.Item returned = GrandExchange.lookup(i);
        if (returned != null && returned.getPrice() != -1)
            return returned.getPrice();
        return 0;
    }
}
