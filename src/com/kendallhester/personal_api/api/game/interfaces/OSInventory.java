package com.kendallhester.personal_api.api.game.interfaces;

import com.gameclient.game.api.hybrid.local.hud.interfaces.Inventory;
import com.gameclient.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.gameclient.game.api.script.Execution;

import java.util.*;

public class OSInventory {

    public static boolean hasAnyOf(Set<String> s) {
        return s != null && Inventory.containsAnyOf(s.toArray(String[]::new));
    }

    /*
     * Interacts
     */

    public static boolean hasItemSelected() {
        return Inventory.getSelectedItem() != null;
    }

    public static boolean deselectItem() {
        if (hasItemSelected()) {
            SpriteItem x = Inventory.getSelectedItem();
            return x != null && x.click() && Execution.delayWhile(OSInventory::hasItemSelected, 200, 300);
        }
        return true;
    }

}
