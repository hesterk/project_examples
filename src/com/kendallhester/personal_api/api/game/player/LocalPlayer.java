package com.kendallhester.personal_api.api.game.player;

import com.kendallhester.personal_api.api.framework.utils.game.Game;
import com.kendallhester.personal_api.api.game.interfaces.Chat;
import com.gameclient.game.api.hybrid.entities.Actor;
import com.gameclient.game.api.hybrid.entities.Npc;
import com.gameclient.game.api.hybrid.entities.Player;
import com.gameclient.game.api.hybrid.entities.details.Locatable;
import com.gameclient.game.api.hybrid.local.Skill;
import com.gameclient.game.api.hybrid.local.hud.interfaces.Health;
import com.gameclient.game.api.hybrid.local.hud.interfaces.Inventory;
import com.gameclient.game.api.hybrid.location.Area;
import com.gameclient.game.api.hybrid.location.Coordinate;
import com.gameclient.game.api.hybrid.region.Npcs;
import com.gameclient.game.api.hybrid.region.Players;
import com.gameclient.game.api.osrs.local.hud.interfaces.Magic;

import java.util.ArrayList;
import java.util.List;

public class LocalPlayer {

    private static Player me;

    public static Player get() {
        if (me == null || !me.isValid()) {
            me = Players.getLocal();
        }

        if (!Game.isLoggedIn(true))
            me = null;

        return me;
    }

    public static boolean isMoving() {
        get();
        return isValid() && (me.isMoving() || me.getAnimationId() != -1);
    }

    public static boolean isValid() {
        get();
        return me != null && me.isValid();
    }

    public static Coordinate getLocation() {
        if (!isValid()) {
            return new Coordinate(0, 0, 0);
        }
        return me.getPosition();
    }

    public static double distanceTo(Locatable x) {
        return getLocation().distanceTo(x);
    }

    public static String getName() {
        if (isValid())
            return me.getName();
        return "";
    }

    public static boolean isAt(Locatable i) {
        return i != null && isContainedIn(i.getArea());
    }

    public static boolean isContainedIn(Area area) {
        if (area != null) {
            return area.contains(getLocation());
        }
        return false;
    }

    public static boolean hasFreeClick() {
        return Inventory.getSelectedItem() == null
                && Magic.getSelected() == null
                && Magic.Lunar.getSelected() == null
                && Magic.Ancient.getSelected() == null
                && Magic.Arceuus.getSelected() == null;
    }

}
