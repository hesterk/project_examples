package com.kendallhester.personal_api.api.game.player;

import com.kendallhester.personal_api.api.framework.utils.game.MouseUtil;
import com.kendallhester.personal_api.api.framework.utils.misc.Sorting;
import com.kendallhester.personal_api.api.framework.utils.misc.Process;
import com.kendallhester.personal_api.api.game.interfaces.OSInventory;
import com.gameclient.game.api.hybrid.Environment;
import com.gameclient.game.api.hybrid.input.Keyboard;
import com.gameclient.game.api.hybrid.local.hud.interfaces.Inventory;
import com.gameclient.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.gameclient.game.api.hybrid.util.StopWatch;
import com.gameclient.game.api.script.Execution;

import java.util.*;
import java.util.concurrent.Callable;

import static java.awt.event.KeyEvent.VK_SHIFT;

public class Actions {

    public static boolean drop(SpriteItem i) {
        return drop(i, null);
    }

    public static boolean drop(SpriteItem i, StopWatch watch) {
        if (i != null && i.isValid()) {
            if (watch == null) watch = new StopWatch();
            if (!watch.isRunning()) watch.start();

            if (Process.delayUntil(
                    () -> i.interact("Drop"),
                    () -> !i.isValid() || MouseUtil.wasClickSuccessful(i),
                    400,
                    500
            ))
                Execution.delayWhile(i::isValid, 238,412);

            if (OSInventory.hasItemSelected()) OSInventory.deselectItem();
            if (i.isValid() && !MouseUtil.wasClickSuccessful(i) && watch.getRuntime() < 10000) {
                try { // retry
                    drop(i, watch);
                } catch(StackOverflowError e) {
                    Objects.requireNonNull(Environment.getBot()).getLogger().severe("SOFE when normal dropping.");
                }
            }

            watch.stop();
            if (watch.getRuntime() >= 10000 && i.isValid()) {
                Objects.requireNonNull(Environment.getBot()).getLogger().debug("Quitting out of drop-method.");
            }
            boolean success = (!i.isValid() || MouseUtil.wasClickSuccessful(i));
            if (success) Execution.delay(50,100); // delay if we were successful, so as not to spam.
            return success;
        }
        return false;
    }

    public static boolean shiftDrop(SpriteItem i, boolean depress) {
        return shiftDrop(i, depress, null);
    }

    public static boolean shiftDrop(SpriteItem i, boolean depress, StopWatch watcher) {
        if (i != null && i.isValid()) {

            if (watcher == null) watcher = new StopWatch();
            if (!watcher.isRunning()) watcher.start();

            if (!Keyboard.isPressed(VK_SHIFT))
                Keyboard.pressKey(VK_SHIFT);

            if (Keyboard.isPressed(VK_SHIFT)) {
                if (MouseUtil.click(i))
                    Execution.delayUntil(() -> MouseUtil.wasClickSuccessful(i) || !i.isValid(), 500, 600);

                Execution.delayWhile(i::isValid, 238,412);
                Execution.delayWhile(() -> i.isValid() && !MouseUtil.wasClickSuccessful(i), 300, 500);

                if (OSInventory.hasItemSelected()) OSInventory.deselectItem();

                if (i.isValid() && !MouseUtil.wasClickSuccessful(i) && watcher.getRuntime() < 10000) {
                    try {
                        return shiftDrop(i, depress); // retry because we miss-clicked. max of 10 seconds per try.
                    } catch (StackOverflowError e) {
                        System.out.println("SOFE with dropping.");
                    }
                }
                watcher.stop();
                if (watcher.getRuntime() >= 10000 && i.isValid()) {
                    Objects.requireNonNull(Environment.getBot()).getLogger().debug("Quitting out of drop-method.");
                }
                boolean success = (!i.isValid() || MouseUtil.wasClickSuccessful(i)) && (!depress || !Keyboard.isPressed(VK_SHIFT) || Keyboard.releaseKey(VK_SHIFT));
                if (success) Execution.delay(50,100); // delay if we were successful, so as not to spam.
                return success;
            } else {
                return drop(i);
            }
        }
        return false;
    }

    public static boolean dropAll(boolean shift, Comparator<SpriteItem> sorter, Callable<Boolean> breakCondition, String... items) {
        boolean done = false;

        if (items.length > 0) {
            List<SpriteItem> results = Inventory.getItems(items).sort(sorter).asList();
            if (shift) {
                for (SpriteItem sp : results) {
                    shiftDrop(sp, false);
                    try {
                        done = breakCondition.call();
                        if (done)
                            break;
                    } catch (Exception ignored) {
                    }
                }
                if (Keyboard.isPressed(VK_SHIFT)) Keyboard.releaseKey(VK_SHIFT);
            } else {
                for (SpriteItem s : results) {
                    Actions.drop(s);
                    try {
                        done = breakCondition.call();
                        if (done) break;
                    } catch (Exception ignored) {
                    }
                }
            }

            return done || (!Inventory.containsAnyOf(items) && (!shift || !Keyboard.isPressed(VK_SHIFT) || Keyboard.releaseKey(VK_SHIFT)));
        }
        return false;
    }

    public static boolean dropAll(boolean shift, Comparator<SpriteItem> sorter, String... items) {
        return dropAll(shift, sorter, null, items);
    }

    public static boolean dropAll(boolean shift, Comparator<SpriteItem> sorter, Set<String> items) {
        return dropAll(shift, sorter, null, items.toArray(String[]::new));
    }

}
