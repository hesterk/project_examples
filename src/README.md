////////////////

The following code is a consolidated, generic sample of the automated personal projects that I write for a public client who 
develops for the MMORPG RuneScape. 

NOTE: This project only represents a small sample of the public work that I have released on this client's website.
Since a lot of the released work is still in production and currently being used, I have chosen to omit a size-able chunk of the code from my
public repositories - though I am more than happy to send links to private repositories, or demonstrate some of the more in-depth
projects (and performance) upon request.

Thanks,

Kendall Hester

hesterkendalle@gmail.com

////////////////