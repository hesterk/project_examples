////////////////

The following code is a consolidated, generic sample of the automated personal projects that I write for a public client who develops for the MMORPG RuneScape.

NOTE: This project only represents a small sample of the public work that I have released on this client's website. Since a lot of the released work is still in production and currently being used, I have chosen to omit a size-able chunk of the code from my public repositories - though I am more than happy to send links to private repositories, or demonstrate some of the more in-depth projects (and performance) upon request.

Thanks,

Kendall Hester

hesterkendalle@gmail.com

____________________________________________

Quick Links:

Looping Structure (design): com/kendallhester/personal_api/api/framework/bots/looping/APILoopingStructure.java

TreeStructure (design): com/kendallhester/personal_api/api/framework/bots/tree/framework/

Looping Structure (ui): com/kendallhester/personal_api/api/framework/bots/universal/ui/APIBotUI.java

Main example class: com/kendallhester/bots/tree_structures/mining_example/main/Mining.java

Examples of asynchronous programming (Futures, Callables, Booleans): 
Should be able to be found in all classes, but an example of a structure designed around them:
com/kendallhester/personal_api/api/framework/utils/misc/Process.java

////////////////